#include <math.h>
#include <stdio.h>
#include <windows.h>
#include "audio.h"
#include "world.h"
#include "render.h"
#include "entity.h"
#include "main.h"
#include "init.h"
#include "engine.h"

#define PIOVER180 0.0174532925
#define PI 3.14159265359
#define INFINITY 999
#define PLAYERHBSIZE 0.4
#define HBSIZE 0.275
#define DEATHVEL 0.000000024

//this file executes all game logic, initialization, resource loading, and rendering methods

//world collisions
int wCollide(double x, double z, double size, struct world *w, int bullet,int node) {

	int fx = (ceil(x * 2) / 2) * 2;
	int fz = (ceil(z * 2) / 2) * 2;

	if (fx == (*w).w)
		fx--;
	if (fz == (*w).h)
		fz--;
	if (fx == 0)
		fx++;
	if (fz == 0)
		fz++;

	for (int a = fx - 1; a < fx + 1; a++)
		for (int b = fz - 1; b < fz + 1; b++) {
			if (x >= ((a*0.5) - size) && x <= ((a*0.5) + size) && (z >= (b*0.5) - size) && (z <= ((b*0.5) + size))) {
				if ((*w).tile[a][b] == 18 && node == 1) {
					continue;
				}
				if ((*w).tile[a][b] > 0 && (*w).tile[a][b] < 59) {		
					if ((*w).tile[a][b] == 12 && bullet == 1) {
						(*w).tile[a][b] = 15;
					}
					else if ((*w).tile[a][b] == 14 && bullet == 1) { //door lock has been destroyed
						//loop through world and destroy all locked doors
						for (int c = 0; c < (*w).w; c++)
							for (int d = 0; d < (*w).h; d++)
								if ((*w).tile[c][d] == 18)
									(*w).tile[c][d] = 0;
						(*w).tile[a][b] = 15;
					}
					else if ((*w).tile[a][b] == 13 && bullet == 1) {
						(*w).tile[a][b] = 16;
					}
					return 1;
				}
				else if ((*w).tile[a][b] == 92) {
					return 2;
				}
			} 
		}
	return 0;
}

//main game loop
void update(struct audioCache audio,struct textureCache *t, struct player *p, struct enemy **e, struct node **n, struct rocket **r, struct world *w, struct item **i) {

	//debug info
	int debug = 0;
	if (GetKeyState(70) < -100) //debug information (f)
		debug = 1;

	if ((*p).health <= 0) {
		(*p).weapon = -1;
		(*p).msc += (*p).msd;
		(*p).ypos = (*p).msc * DEATHVEL;
		if ((*p).ypos >= 0.18) {
			(*p).ypos = 0.18;
		}
	}
	else {


		if ((*p).score > (*p).lscore + 20000) {
			(*p).lscore += 20000;
			(*p).lives++;
		}

		//make sure angle never exceeds 360
		if ((*p).theta > 360)
			(*p).theta = (*p).theta - 360;
		else if ((*p).theta < 0)
			(*p).theta = (*p).theta + 360;

		//move the player based on input
		double moveAngle = 0, rads = 0;

		if ((*p).in1 != 0) {
			if ((*p).in2 != 0) {
				if ((*p).in1 == 1 && (*p).in2 == 2) // w , s
					moveAngle = ((*p).theta + 90); //ignore the s
				else if ((*p).in1 == 1 && (*p).in2 == 3) // w , a
					moveAngle = ((*p).theta + 45);
				else if ((*p).in1 == 1 && (*p).in2 == 4) //w , d
					moveAngle = ((*p).theta + 135);
				else if ((*p).in1 == 2 && (*p).in2 == 3) //s , a
					moveAngle = ((*p).theta + 315);
				else if ((*p).in1 == 2 && (*p).in2 == 4) //s , d
					moveAngle = ((*p).theta + 225);
				else if ((*p).in1 == 3 && (*p).in2 == 4) //a , d
					moveAngle = ((*p).theta); //ignore the d
			}
			else {
				switch ((*p).in1) {
				case 1: //w
					moveAngle = ((*p).theta + 90);
					break;
				case 2: //s
					moveAngle = ((*p).theta + 270);
					break;
				case 3: //a
					moveAngle = ((*p).theta);
					break;
				case 4: //d
					moveAngle = ((*p).theta + 180);
					break;
				}
			}
			rads = (moveAngle)*PIOVER180;
			(*p).x += (*p).vel * cos(rads);
			(*p).z += (*p).vel * sin(rads);
		}

		(*p).collision = wCollide((*p).x, (*p).z, PLAYERHBSIZE, w,0,0);


		//change movement if colliding with world
		if ((*p).collision == 1) {

			if (moveAngle >= 360)
				moveAngle -= 360;

			//roll back movements
			(*p).x = (*p).lastx;
			(*p).z = (*p).lastz;

			//correct movements into valid operations using vector projection
			if ((moveAngle >= 0 && moveAngle < 45) || (moveAngle >= 135 && moveAngle < 180))
				moveAngle = 90;
			else if ((moveAngle >= 45 && moveAngle < 90) || (moveAngle >= 315 && moveAngle < 360))
				moveAngle = 0;
			else if ((moveAngle >= 90 && moveAngle < 135) || (moveAngle >= 180 && moveAngle < 270))
				moveAngle = 180;
			else if ((moveAngle >= 135 && moveAngle < 180) || (moveAngle >= 270 && moveAngle < 315))
				moveAngle = 270;

			//move the player again
			rads = (moveAngle)*PIOVER180;
			(*p).x += (*p).vel * cos(rads);
			(*p).z += (*p).vel * sin(rads);

			//check collisions again
			(*p).collision = wCollide((*p).x, (*p).z, PLAYERHBSIZE, w,0, 0);

			//stop movement if still colliding with world
			if ((*p).collision) {
				(*p).x = (*p).lastx;
				(*p).z = (*p).lastz;
			}
		}
		else if ((*p).collision == 2) {
			//level transition, change the map
			(*p).exit = 1;
		}

		(*p).ma = moveAngle;

		//attack routines
		if ((*p).attack == 1) {
			for (int a = 0; a < (*w).enemyc; a++) {
				switch ((*p).weapon) {
				case 0:
					if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, ((*p).theta*PIOVER180) + 4.7, w, 0, 1) == 1) {
						if (0.5 > sqrt(((-(*e)[a].x - (*p).x) * (-(*e)[a].x - (*p).x)) + ((-(*e)[a].z - (*p).z) * (-(*e)[a].z - (*p).z)))) {
							printf("You hit enemy %d\n", a);
							(*e)[a].health -= 10;
						}
					}
					break;
				case 1:
					if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, ((*p).theta*PIOVER180) + 4.7, w, 0, 1) == 1) {
						printf("You hit enemy %d\n", a);
						(*e)[a].health -= 14;
					}
					break;
				case 2:
					for (float b = -6; b < 6; b += 2) {
						if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, ((*p).theta*PIOVER180) + 4.7 - (b*PIOVER180), w, 0, 1) == 1) {
							printf("You hit enemy %d\n", a);
							(*e)[a].health -= 12;
						}
					}
					break;
				case 3:
					if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, ((*p).theta*PIOVER180) + 4.7, w, 0, 1) == 1) {
						printf("You hit enemy %d\n", a);
						(*e)[a].health -= 8;
					}
					break;
				}
			}
			(*p).attack = -1;
			(*p).guntexture = 1;
		}

		if ((*p).attack == -1) {
			if ((*p).msc == 0) {
				switch ((*p).weapon) {
				case 1:
					(*p).ammo[0]--;
					break;
				case 2:
					(*p).ammo[1]--;
					break;
				case 3:
					(*p).ammo[2]--;
					break;
				case 4:
					(*p).ammo[3]--;
					break;
				}
			}
			(*p).msc += (*p).msd;
			switch ((*p).weapon) {
			case 0:
				if ((*p).msc < 2000000) {
					(*p).guntexture = 1;
				}
				else if ((*p).msc > 2000000 && (*p).msc < 4000000) {
					(*p).guntexture = 0;
				}
				else if ((*p).msc > 4000000) {
					(*p).guntexture = 0;
					(*p).msc = 0;
					(*p).attack = 0;
				}
				break;
			case 1:
				if ((*p).msc > 2500000 && (*p).msc < 5000000) {
					(*p).guntexture = 2;
				}
				else if ((*p).msc > 5000000 && (*p).msc < 7500000) {
					(*p).guntexture = 0;
				}
				else if ((*p).msc > 7500000) {
					(*p).msc = 0;
					(*p).attack = 0;
					(*p).guntexture = 0;
				}
				break;
			case 2:
				if ((*p).msc > 2500000 && (*p).msc < 5000000) {
					(*p).guntexture = 2;
				}
				else if ((*p).msc > 5000000 && (*p).msc < 7500000) {
					(*p).guntexture = 0;
				}
				else if ((*p).msc > 7500000 && (*p).msc < 10000000) {
					(*p).guntexture = 3;
				}
				else if ((*p).msc > 10000000) {
					(*p).msc = 0;
					(*p).attack = 0;
					(*p).guntexture = 0;
				}
				break;
			case 3:
				if ((*p).msc > 800000 && (*p).msc < 1600000) {
					(*p).guntexture = 2;
				}
				else if ((*p).msc > 1600000) {
					(*p).msc = 0;
					(*p).attack = 0;
					(*p).guntexture = 0;
				}
				break;
			case 4:
				if ((*p).msc > 1000000) {
					(*p).msc = 0;
					(*p).attack = 0;
					(*r)[0].texture = 0;
				}
				break;
			}
		}
	}

		//node routines
		for (int a = 0; a < (*w).nodec; a++) {
			double theta = atan2(((*n)[a].z - (*p).z), ((*n)[a].x - (*p).x));
			(*n)[a].player = vecTest((*n)[a].x, (*n)[a].z, (*p).x, (*p).z, theta, w, 1, 0);
		}

		//item routines
		for (int a = 0; a < (*w).itemc; a++) {
			if ((*i)[a].taken == 0) {
				if (nCollide((*p).x, (*p).z, (*i)[a].x, (*i)[a].z) == 1) {
					switch ((*i)[a].type) {
					case 0:
						(*p).score += 100;
						(*i)[a].taken = 1;
						break;
					case 1:
						(*p).ammo[0] += 4;
						(*i)[a].taken = 1;
						break;
					case 2:
						(*p).ammo[1] += 3;
						(*i)[a].taken = 1;
						break;
					case 3:
						(*p).ammo[2] += 30;
						(*i)[a].taken = 1;
						break;
					case 4:
						(*p).health += 50;
						(*i)[a].taken = 1;
						break;
					case 5:
						(*p).armor += 100;
						(*i)[a].taken = 1;
						break;
					case 6:
						(*p).ammo[3] += 2;
						(*i)[a].taken = 1;
					}
				}
			}
		}

		//rocket routines
		if ((*p).attack == 2 && (*p).weapon == 4) {
			(*r)[0].x = (*p).x;
			(*r)[0].z = (*p).z;
			(*r)[0].x += 0.25 * cos(((*p).theta*PIOVER180) - 4.7);
			(*r)[0].z += 0.25 * sin(((*p).theta*PIOVER180) - 4.7);
			(*r)[0].theta = ((*p).theta*PIOVER180) - 4.7;
			(*r)[0].texture = 1;
			(*p).attack = 3;
		}
		else if ((*p).attack == 3) {

			(*r)[0].texture = 1;

			//move forwards
			(*r)[0].x += ((*p).vel * 5) * cos((*r)[0].theta);
			(*r)[0].z += ((*p).vel * 5) * sin((*r)[0].theta);

			//printf("%f,%f\n", (*r)[0].x, (*r)[0].z);

			if (wCollide((*r)[0].x, (*r)[0].z, HBSIZE, w, 0, 0) == 1) {
				(*p).attack = -1;
				(*r)[0].texture = 2;
				if (1 > sqrt((((*r)[0].x - (*p).x) * ((*r)[0].x - (*p).x)) + (((*r)[0].z - (*p).z) * ((*r)[0].z - (*p).z)))) {
					for (int a = 0; a < (*w).enemyc; a++)
						(*e)[a].health -= (int)trunc(100 - (sqrt((((*r)[0].x - (*p).x) * ((*r)[0].x - (*p).x)) + (((*r)[0].z - (*p).z) * ((*r)[0].z - (*p).z))) * 100));
				}

			}
			for (int a = 0; a < (*w).enemyc; a++) {
				if (nCollide((*r)[0].x, (*r)[0].z, -(*e)[a].x, -(*e)[a].z) == 1) {
					(*p).attack = -1;
					(*r)[a].texture = 2;
					(*e)[a].health -= 100;
				}
			}
		}

		for (int a = 1; a < (*w).rocketc; a++) {

			if ((*e)[(*r)[a].owner].mode != 4) {

				if ((*e)[(*r)[a].owner].attack == 0) {
					(*r)[a].x = -(*e)[(*r)[a].owner].x;
					(*r)[a].z = -(*e)[(*r)[a].owner].z;
					(*r)[a].theta = (*e)[(*r)[a].owner].sTheta;
					(*r)[a].texture = 0;
				}

				if ((*e)[(*r)[a].owner].attack == 1) {

					(*r)[a].x = -(*e)[(*r)[a].owner].x;
					(*r)[a].z = -(*e)[(*r)[a].owner].z;
					(*r)[a].theta = (*e)[(*r)[a].owner].sTheta;
					(*e)[(*r)[a].owner].attack = 4;
					(*r)[a].texture = 1;
				}
				else if ((*e)[(*r)[a].owner].attack == 4) {

					(*r)[a].texture = 1;

					//move forwards
					(*r)[a].x += ((*p).vel * 2) * cos((*r)[a].theta);
					(*r)[a].z += ((*p).vel * 2) * sin((*r)[a].theta);


					//printf("%f,%f\n", (*r)[a].x, (*r)[a].z);

					if (wCollide((*r)[a].x, (*r)[a].z, HBSIZE, w, 0, 0) == 1) {
						(*e)[(*r)[a].owner].attack = 5;
						(*r)[a].texture = 2;
						if (1 > sqrt((((*r)[a].x - (*p).x) * ((*r)[a].x - (*p).x)) + (((*r)[a].z - (*p).z) * ((*r)[a].z - (*p).z)))) {
							printf("You got hit by a rocket! \n");
							if ((*p).armor > 0)
								(*p).armor -= (int)trunc(90 - (sqrt((((*r)[a].x - (*p).x) * ((*r)[a].x - (*p).x)) + (((*r)[a].z - (*p).z) * ((*r)[a].z - (*p).z))) * 100));
							else
								(*p).health -= (int)trunc(90 - (sqrt((((*r)[a].x - (*p).x) * ((*r)[a].x - (*p).x)) + (((*r)[a].z - (*p).z) * ((*r)[a].z - (*p).z))) * 100));
						}

					}
					else if (nCollide((*r)[a].x, (*r)[a].z, (*p).x, (*p).z) == 1) {
						(*e)[(*r)[a].owner].attack = 5;
						(*r)[a].texture = 2;
						printf("You got hit directly by a rocket! \n");
						if ((*p).armor > 0)
							(*p).armor -= 90;
						else
							(*p).health -= 90;
					}
				}
				else if ((*e)[(*r)[a].owner].attack == 5) {
					if ((*e)[(*r)[a].owner].health > 0)
						(*r)[a].texture = 2;
				}
				else {
					(*r)[a].texture = 0;
				}
				if ((*e)[(*r)[a].owner].attack == 5 && (*e)[(*r)[a].owner].msc > 5000000)
					(*r)[a].texture = 0;
			}
		}

		//enemy AI
		for (int a = 0; a < (*w).enemyc; a++) {

			(*e)[a].lastx = (*e)[a].x;
			(*e)[a].lastz = (*e)[a].z;

			//velocity is relative to frame draw time
			(*e)[a].vel = 0.0000001 * (*p).msd;

			switch ((*e)[a].mode) {
				//idle state - no enemies known
			case 0:
				//see if player becomes visible
				(*e)[a].theta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x));

				(*e)[a].seeThing = vecTest(-(*e)[a].x, -(*e)[a].z, (*p).x, (*p).z, (*e)[a].theta, w, 0, 0);

				if ((*e)[a].seeThing == 1) {
					(*e)[a].mode = 1;
				}

				if ((*e)[a].health < 0) {
					if ((*e)[a].weapon == 1) {
						(*p).score += 250;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 2) {
						(*p).score += 500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 3) {
						(*p).score += 1000;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 4) {
						(*p).score += 1500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						for (int b; b < (*w).rocketc; b++) {
							if ((*r)[b].owner == a)
								(*r)[b].texture = 0;
						}
					}
					else if ((*e)[a].weapon == 5) {
						(*p).score += 3000;
					}
					(*e)[a].mode = 4;
				}
				break;
				//attack state - player is in attack range
			case 1:

				if ((*e)[a].attack == 1)
					(*e)[a].attack = 2;

				//wait before attacking
				(*e)[a].msc += (*p).msd;
				switch ((*e)[a].weapon) {
				case 1:
				case 2:
					if ((*e)[a].msc > 25000000 && (*e)[a].attack == 2) {
						(*e)[a].texture = 0;
						(*e)[a].msc = 0;
						(*e)[a].attack = 0;
					}
					//after 2 seconds fire at the player
					else if ((*e)[a].msc > 20000000 && (*e)[a].attack == 3) {
						(*e)[a].attack = 1;
						(*e)[a].texture = 1;
					}
					else if ((*e)[a].msc > 10000000 && (*e)[a].attack == 0) {
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					break;
				case 3: 
					if ((*e)[a].msc > 2500000 && (*e)[a].attack == 2) {
						(*e)[a].texture = 2;
						(*e)[a].msc = 0;
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					//after 0.5 seconds fire at the player
					else if ((*e)[a].msc > 2000000 && (*e)[a].attack == 3) {
						(*e)[a].attack = 1;
						(*e)[a].texture = 1;
					}
					else if ((*e)[a].msc > 12500000 && (*e)[a].attack == 0) {
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					break;
				case 5: //jihadi john
					if ((*e)[a].msc > 2500000 && (*e)[a].attack == 2) {
						(*e)[a].texture = 2;
						(*e)[a].msc = 0;
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					//after 0.5 seconds fire at the player
					else if ((*e)[a].msc > 1000000 && (*e)[a].attack == 3) {
						(*e)[a].attack = 1;
						(*e)[a].texture = 1;
					}
					else if ((*e)[a].msc > 12500000 && (*e)[a].attack == 0) {
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					break;
				case 4:
				case 6: //khattab
					if ((*e)[a].attack == 5 && (*e)[a].texture == 1) {
						(*e)[a].texture = 0;
						(*e)[a].msc = 0;
					
					}
					else if ((*e)[a].attack == 5 && (*e)[a].msc > 10000000) {
						(*e)[a].texture = 0;
						(*e)[a].msc = 0;
						(*e)[a].attack = 0;
					}
					//after 3 seconds fire at the player
					else if ((*e)[a].msc > 30000000 && (*e)[a].attack == 4) {
						(*e)[a].texture = 0;
					}
					else if ((*e)[a].msc > 10000000 && (*e)[a].attack == 0) {
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 1;
						(*e)[a].texture = 1;
					}
					break;
				case 7:
					if ((*e)[a].msc > 2500000 && (*e)[a].attack == 2) {
						(*e)[a].texture = 2;
						(*e)[a].msc = 0;
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					//after 0.5 seconds fire at the player
					else if ((*e)[a].msc > 1000000 && (*e)[a].attack == 3) {
						(*e)[a].attack = 1;
						(*e)[a].texture = 1;
					}
					else if ((*e)[a].msc > 20000000 && (*e)[a].attack == 0) {
						(*e)[a].sTheta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x)) + PI;
						(*e)[a].attack = 3;
					}
					break;
				}
				if ((*e)[a].attack == 1) {
					switch ((*e)[a].weapon) {
					case 1:
						if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, (*e)[a].sTheta, w, 0, 1) == 1) {
							if ((*p).armor > 0)
								(*p).armor -= 14;
							else
								(*p).health -= 14;
						}
						break;
					case 2:
						for (float b = -6; b < 6; b += 2) {
							if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, (*e)[a].sTheta - (b*PIOVER180), w, 0, 1) == 1) {
								if ((*p).armor > 0)
									(*p).armor -= 12;
								else
									(*p).health -= 12;
							}
						}
					case 3:
						if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, (*e)[a].sTheta, w, 0, 1) == 1) {
							if ((*p).armor > 0)
								(*p).armor -= 4;
							else
								(*p).health -= 4;
						}
						break;
					case 5:
						if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, (*e)[a].sTheta, w, 0, 1) == 1) {
							if ((*p).armor > 0)
								(*p).armor -= 8;
							else
								(*p).health -= 8;
						}
						break;
					case 4:
					case 6:
						
						break;
					case 7:
						for (float b = -6; b < 6; b += 2) {
							if (vecTest((*p).x, (*p).z, -(*e)[a].x, -(*e)[a].z, (*e)[a].sTheta - (b*PIOVER180), w, 0, 1) == 1) {
								if ((*p).armor > 0)
									(*p).armor -= 2;
								else
									(*p).health -= 2;
							}
						}
						break;
					}
				}

				if ((*e)[a].weapon == 2 || (*e)[a].weapon == 5 || (*e)[a].weapon == 6) {
					(*e)[a].pdist = sqrt(((-(*e)[a].x - (*p).x) * (-(*e)[a].x - (*p).x)) + ((-(*e)[a].z - (*p).z) * (-(*e)[a].z - (*p).z)));
				}

				//see if player is no longer visible
				(*e)[a].theta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x));

				(*e)[a].seeThing = vecTest(-(*e)[a].x, -(*e)[a].z, (*p).x, (*p).z, (*e)[a].theta, w, 0, 0);

				if ((*e)[a].seeThing == 0) {

					if ((*e)[a].weapon == 5 || (*e)[a].weapon == 3 || (*e)[a].weapon == 4 || (*e)[a].weapon == 6 || (*e)[a].weapon == 7) {
						(*e)[a].texture = 0;
						(*e)[a].attack = 0;
					}

					if ((*e)[a].weapon == 4 || (*e)[a].weapon == 6) {
						for (int b; b < (*w).rocketc; b++) {
							if ((*r)[b].owner == a)
								(*r)[b].texture = 0;
						}
					}
					
					(*e)[a].mode = 2;
					for (int b = 0; b < (*w).nodec; b++) {
						(*e)[a].theta = atan2((-(*e)[a].z - (*n)[b].z), (-(*e)[a].x - (*n)[b].x));
						if (vecTest(-(*e)[a].x, -(*e)[a].z, (*n)[b].x, (*n)[b].z, (*e)[a].theta, w, 0, 0) == 1) {
							(*e)[a].target = b;
							printf("Going to node %d\n", b);
							break;
						}
					}
				}

				if ((*e)[a].health < 0) {
					if ((*e)[a].weapon == 1) {
						(*p).score += 250;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						//printf("Item %d of type %d at ( %f, %f )", a, (*i)[a].type, (*i)[a].x, (*i)[a].z);
					}
					else if ((*e)[a].weapon == 2) {
						(*p).score += 500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						//printf("Item %d of type %d at ( %f, %f )", a, (*i)[a].type, (*i)[a].x, (*i)[a].z);
					}
					else if ((*e)[a].weapon == 3) {
						(*p).score += 1000;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 4) {
						(*p).score += 1500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						for (int b; b < (*w).rocketc; b++) {
							if ((*r)[b].owner == a)
								(*r)[b].texture = 0;
						}
					}
					else if ((*e)[a].weapon == 5) {
						(*p).score += 4000;
					}
					
					(*e)[a].mode = 4;
				}
				else if (((*e)[a].weapon == 2 && (*e)[a].pdist > 1) || ((*e)[a].weapon == 5 && (*e)[a].pdist > 3) || ((*e)[a].weapon == 6 && (*e)[a].pdist > 3)) { //if shotgun enemy or Jihadi John or Khattab, move towards the player 

					//move forwards
					(*e)[a].x += (*e)[a].vel * cos((*e)[a].theta);
					(*e)[a].z += (*e)[a].vel * sin((*e)[a].theta);

					//collision routines
					(*e)[a].collision = wCollide(-(*e)[a].x, -(*e)[a].z, PLAYERHBSIZE, w,0, 0);

					if ((*e)[a].collision) {

						//roll back movements
						(*e)[a].x = (*e)[a].lastx;
						(*e)[a].z = (*e)[a].lastz;

					}
				}
				break;
				//find the first node
			case 2:

				//see if player becomes visible
				(*e)[a].theta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x));

				(*e)[a].seeThing = vecTest(-(*e)[a].x, -(*e)[a].z, (*p).x, (*p).z, (*e)[a].theta, w, 0, 0);

				if ((*e)[a].seeThing == 1) {
					(*e)[a].mode = 1;
				}

				//look at the target
				(*e)[a].theta = atan2((-(*e)[a].z - (*n)[(*e)[a].target].z), (-(*e)[a].x - (*n)[(*e)[a].target].x));

				//movement
				(*e)[a].x += (*e)[a].vel * cos((*e)[a].theta);
				(*e)[a].z += (*e)[a].vel * sin((*e)[a].theta);

				//collision routines
				(*e)[a].collision = wCollide(-(*e)[a].x, -(*e)[a].z, PLAYERHBSIZE, w,0, 0);

				if ((*e)[a].collision) {

					//roll back movements
					(*e)[a].x = (*e)[a].lastx;
					(*e)[a].z = (*e)[a].lastz;

				}

				//check if reached target node, then calculate the path
				(*e)[a].collision = nCollide(-(*e)[a].x, -(*e)[a].z, (*n)[(*e)[a].target].x, (*n)[(*e)[a].target].z);

				if ((*e)[a].collision == 1) {
					int pNode;
					for (int b = 0; b < (*w).nodec; b++) {
						if ((*n)[b].player == 1) {
							pNode = b;
							break;
						}
					}
						
						if ((*e)[a].pathMEM == 1)
							free((*e)[a].path);
						(*e)[a].path = getPath(*n, (*e)[a].target, pNode, (*w).nodec, &(*e)[a].pathLEN);

						printf("Found path from %d to %d\n", (*e)[a].target,pNode);

						(*e)[a].path[(*e)[a].pathLEN] = pNode;

						(*e)[a].pathLEN++;

						printf("LEN : %d\n", (*e)[a].pathLEN);


						for (int b = 0; b < (*e)[a].pathLEN; b++)
							printf("%d,", (*e)[a].path[b]);
						printf("\n");


						(*e)[a].target = (*e)[a].path[0];
						(*e)[a].mode = 3;
						(*e)[a].pathC = 1;

						if ((*e)[a].pathMEM == 0)
							(*e)[a].pathMEM = 1;
						
						
					
				}

				if ((*e)[a].health < 0) {
					if ((*e)[a].weapon == 1) {
						(*p).score += 250;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						//printf("Item %d of type %d at ( %f, %f )", a, (*i)[a].type, (*i)[a].x, (*i)[a].z);
					}
					else if ((*e)[a].weapon == 2) {
						(*p).score += 500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						//printf("Item %d of type %d at ( %f, %f )", a, (*i)[a].type, (*i)[a].x, (*i)[a].z);
					}
					else if ((*e)[a].weapon == 3) {
						(*p).score += 1000;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 4) {
						(*p).score += 1500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 5) {
						(*p).score += 4000;
					}
					(*e)[a].mode = 4;

				}

				break;
				//seach state - find the player
			case 3:

				//see if player becomes visible
				(*e)[a].theta = atan2((-(*e)[a].z - (*p).z), (-(*e)[a].x - (*p).x));

				(*e)[a].seeThing = vecTest(-(*e)[a].x, -(*e)[a].z, (*p).x, (*p).z, (*e)[a].theta, w, 0, 0);

				if ((*e)[a].seeThing == 1) {
					(*e)[a].mode = 1;
				}

				(*e)[a].theta = atan2((-(*e)[a].z - (*n)[(*e)[a].target].z), (-(*e)[a].x - (*n)[(*e)[a].target].x));

				//movement
				(*e)[a].x += (*e)[a].vel * cos((*e)[a].theta);
				(*e)[a].z += (*e)[a].vel * sin((*e)[a].theta);

				//collision routines
				(*e)[a].collision = wCollide(-(*e)[a].x, -(*e)[a].z, PLAYERHBSIZE, w,0, 0);

				if ((*e)[a].collision == 1) {

					//roll back movements
					(*e)[a].x = (*e)[a].lastx;
					(*e)[a].z = (*e)[a].lastz;

				}

				(*e)[a].collision = nCollide(-(*e)[a].x, -(*e)[a].z, (*n)[(*e)[a].target].x, (*n)[(*e)[a].target].z);

				//reached target node, retrive the next one
				if ((*e)[a].collision == 1) {
					
					//check to see if this is the final destination
					if (((*e)[a].path[(*e)[a].pathC] != 999) || ((*e)[a].pathC < (*w).nodec - 1)) {
						(*e)[a].target = (*e)[a].path[(*e)[a].pathC];
						(*e)[a].pathC++;
					}
					else { //reached the end of the path
						   //see if player becomes visible
						(*e)[a].seeThing = vecTest(-(*e)[a].x, -(*e)[a].z, (*p).x, (*p).z, (*e)[a].theta, w,0,0);
						if ((*e)[a].seeThing == 1) {
							(*e)[a].mode = 1;
						}
						else {
							//find another path
							(*e)[a].mode = 2;
						}
					}
				}

				if ((*e)[a].health < 0) {
					if ((*e)[a].weapon == 1) {
						(*p).score += 250;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 2) {
						(*p).score += 500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 3) {
						(*p).score += 1000;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
					}
					else if ((*e)[a].weapon == 4) {
						(*p).score += 1500;
						//the items that enemies drop share ID's with the enemies
						(*i)[a].x = -(*e)[a].x;
						(*i)[a].z = -(*e)[a].z;
						(*i)[a].taken = 0;
						for (int b; b < (*w).rocketc; b++) {
							if ((*r)[b].owner == a)
								(*r)[b].texture = 0;
						}
					}
					else if ((*e)[a].weapon == 5) {
						(*p).score += 4000;
					}
					(*e)[a].mode = 4;
				}

				break;
				//dead
			case 4:
				if ((*e)[a].weapon == 5 || (*e)[a].weapon == 6 || (*e)[a].weapon == 7)
					(*p).exit = 1;
				break;
			}
		}
	//render graphics
	render(*t, *p, *w, *e, *i,*r, debug);	
}

int vecTest(double x, double z, double tx,double tz, double theta, struct world *w, int n,int b) {

	int wallCollide = 0;

	theta -= PI;

	for (int a = 0; a < 400; a++) {

		x += 0.05 * cos(theta);
		z += 0.05 * sin(theta);

		if (x >= tx - 0.07 && x <= tx + 0.07 && z >= tz - 0.07 && z <= tz + 0.07)
			return 1;

		if(n == 1)
			wallCollide = wCollide(x, z, PLAYERHBSIZE, w,0, 1);
		else 
			wallCollide = wCollide(x, z, HBSIZE, w, 0, 1);

		if (b == 1) {
			wallCollide = wCollide(x, z, HBSIZE, w, 1, 0);
		}

		if (wallCollide == 1)
			return 0;
	}
	return 2; //out of range
}

//node collisions
int nCollide(double x, double z, double tx, double tz) {

	if (x >= tx - 0.1 && x <= tx + 0.1 && z >= tz - 0.1 && z <= tz + 0.1)
		return 1;
	else
		return 0;
}

//find path between source and destination nodes
int *getPath(struct node *n,int src,int dest,int nodec,int *pathL) {

	//intialize variables
	int *flag = malloc(nodec*sizeof(int));
	double *dist = malloc(nodec*sizeof(double));
	int *path = malloc(nodec*sizeof(int));

	for (int a = 0; a < nodec; a++) {
		flag[a] = 0;
		dist[a] = INFINITY;
		path[a] = INFINITY;
	}

	flag[src] = 1;
	dist[src] = 0;
	path[0] = src;

	int cNode = src;
	int tempN = src;
	double cNodeD = 0;
	double min = INFINITY;
	double tempD = INFINITY;
	int pathC = 0;
	int checkVN = 0;

	//find paths
	while (cNode != dest) {

		checkVN = 0;
		min = INFINITY;
		cNodeD = dist[cNode];

		//make sure we didnt get into a dead end
		for (int a = 0; a < n[cNode].memVN; a++) {
			if (flag[n[cNode].vNode[a]] == 1)
				checkVN++;
		}

		//printf("VN's : %d TVNS : %d\n", checkVN, n[cNode].memVN);
		//printf("Starting with cNode=%d cNodeD=%f tempN=%d tempD=%f min=%f memVN=%d\n", cNode, cNodeD, tempN, tempD, min, n[cNode].memVN - 1);

		//loop though all the nodes that the current node is connected to
		for (int b = 0; b <= (n[cNode].memVN - 1); b++) {

			//printf("Checking node # %d , flag[%d]=%d\n", n[cNode].vNode[b], n[cNode].vNode[b], flag[n[cNode].vNode[b]]);

			//check if it is connected to any node IDs that are not flagged
			if (flag[n[cNode].vNode[b]] == 0) {

				//if so get the distance between the current node and the unflagged node
				tempD = cNodeD + n[cNode].vNodeD[b];

				//printf("tempD=%f\n", tempD);

				//check and see if the temp distance between the unflagged node ID is lower than the previously set distance
				if (tempD < dist[n[cNode].vNode[b]]) {

					//store the new distance in the dist array
					dist[n[cNode].vNode[b]] = tempD;
					//update the path
					if (cNode != path[pathC - 1]) {
						path[pathC] = cNode;
						pathC++;
					}

					//printf("dist[%d]=%f\n", n[cNode].vNode[b], tempD);
				}

				//check if a shorter distance than min was set
				if (dist[n[cNode].vNode[b]] < min) {
					//make min the current longest dist
					min = dist[n[cNode].vNode[b]];
					//set temp node ID to current node ID that distance was found for
					tempN = n[cNode].vNode[b];

					//printf("min=%f, tempN=%d\n", min, tempN);

				}
			}
		}

		//printf("Connecting nodes checked!\n");

		if (checkVN == (n[cNode].memVN)) {
			path[pathC] = INFINITY; //roll back the path
			pathC--;
			tempN = path[pathC];

			//printf("Dead End! Going back to %d\n", path[pathC]);
		}

		//update current node and flag it
		cNode = tempN;
		flag[tempN] = 1;

	}

	printf("%d, %d\n", dest,pathC);

	//pathC++;
	path[pathC] = dest;

	*pathL = pathC;

	free(dist);
	free(flag);

	return path;
}

void updateMenu(int input,int *tpos, int *screen,BOOL *bQuit,int *menu) {

	if (input == 3 && *tpos == 0 && *screen == 0) {
		Sleep(250);
		*screen = 3;
	}
	else if (input == 3 && *tpos == 1 && *screen == 0) {
		Sleep(250);
		*screen = 1;
	}
	else if (input == 3 && *tpos == 2 && *screen == 0) {
		Sleep(250);
		*screen = 4;
	}
	else if (input == 3 && *tpos == 2 && *screen == 0) {
		Sleep(250);
		*screen = 5;
	}
	else if (input == 3 && *tpos == 3 && *screen == 0)
		*bQuit = TRUE;
	else if (input == 3 && *screen == 1){
		Sleep(250);
		*screen = 0;
	}
	else if (input == 3 && *screen == 4) {
		Sleep(250);
		*screen = 0;
		*tpos = 2;
	}
	else if (input == 3 && *screen == 3) {
		Sleep(1000);
		*menu = 0;
	}

	if (*screen == 0) {
		if (input == 2) {
			*tpos += 1;
			Sleep(250); //hang at least .5 sec
		}
		else if (input == 1) {
			*tpos -= 1;
			Sleep(250); //hang at least .5 sec
		}
		if (*tpos > 3)
			*tpos = 3;
		if (*tpos < 0)
			*tpos = 0;
	}
	else if (*screen == 4) {
		if (input == 2) {
			*tpos = 2;
			Sleep(250); //hang at least .5 sec
		}
		else if (input == 1) {
			*tpos = 1;
			Sleep(250); //hang at least .5 sec
		}
	}


}

void changeLevel(struct player *p, struct world *w, struct enemy **e, struct node **n, struct item **i,struct rocket **r, int level) {
	(*p).collision = 0;
	if((*p).health < 100)
		(*p).health = 100;
	(*p).x = 1;
	(*p).z = 1;
	(*p).vel = 0.0025;
	(*p).tvel = -0.035;
	(*p).theta = 270;
	(*p).weapon = 1;
	(*p).msd = 0;
	(*p).fps = 0;
	(*p).guntexture = 0;
	(*p).msc = 0;
	(*p).ypos = 0;
	


	free(*i);
	for (int a = 0; a < (*w).enemyc; a++)
		alDeleteSources(1, &(*e)[a].source);
	free(*e);
	free(*r);
	freeGraph(*n, *w);
	freeWorld(*w);


	switch (level) {
	case 1:
		(*w) = readWorld("data/maps/p1a1.map");
		break;
	case 2:
		(*w) = readWorld("data/maps/p1a2.map");
		break;
	case 3:
		(*w) = readWorld("data/maps/p1a3.map");
		break;
	case 4:
		(*w) = readWorld("data/maps/p1a4.map");
		break;
	case 5:
		(*w) = readWorld("data/maps/p1a5.map");
		break;
	case 6:
		(*w) = readWorld("data/maps/p1a6.map");
		break;
	case 7:
		(*w) = readWorld("data/maps/p2a1.map");
		break;
	case 8:
		(*w) = readWorld("data/maps/p2a2.map");
		break;
	case 9:
		(*w) = readWorld("data/maps/p2a3.map");
		break;
	case 10:
		(*w) = readWorld("data/maps/p2a4.map");
		break;
	case 11:
		(*w) = readWorld("data/maps/p2a5.map");
		break;
	case 12:
		(*w) = readWorld("data/maps/p2a6.map");
		break;
	case 13:
		(*w) = readWorld("data/maps/p2a7.map");
		break;
	case 14:
		(*w) = readWorld("data/maps/p2a8.map");
		break;
	case 15:
		(*w) = readWorld("data/maps/p3a1.map");
		break;
	case 16:
		(*w) = readWorld("data/maps/p3a2.map");
		break;
	case 17:
		(*w) = readWorld("data/maps/p3a3.map");
		break;
	case 18:
		(*w) = readWorld("data/maps/p3a4.map");
		break;
	case 19:
		(*w) = readWorld("data/maps/p3a5.map");
		break;
	case 20:
		(*w) = readWorld("data/maps/p3a6.map");
		break;
	case 21:
		(*w) = readWorld("data/maps/p3a7.map");
		break;
	case 22:
		(*w) = readWorld("data/maps/p3a8.map");
		break;
	case 23:
		(*w) = readWorld("data/maps/p3a9.map");
		break;
	case 24:
		(*w) = readWorld("data/maps/p3a10.map");
		break;
	case 25:
		break;
	}

	*e = initalizeEnemy(w);

	*i = initalizeItems(w, *e);

	*r = initalizeRocket(e, w);

	for (int a = 0; a < (*w).enemyc; a++)
		alGenSources(1, &(*e)[a].source);

	*n = createGraph(w);
}