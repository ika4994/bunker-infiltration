#include <al.h>

#define ENTITY_H_INCLUDED

//structures
struct player {
	int collision , in1 , in2, attack, msc, guntexture, usrc,score;
	int lives, health, armor, weapon,exit, *ammo;
	int level;
	int hasgun[4];
	int hasKey[2];
	INT64 msd;
	double x, z, theta, lastx,lastz,vel,tvel, ma, fps, ypos;
	ALuint source;
	int lscore;
};

struct enemy {
	int collision, health, weapon, mode, seeThing, target, pathC, texture, msc, attack, pathMEM, pathLEN;
	int *path;
	double x, z, theta, lastx, lastz, vel, tvel, sTheta,pdist;
	int rocket;
	ALuint source;
};

struct node {
	double x, z;
	int player;
	int *vNode;
	int memVN;
	double *vNodeD;
};

struct item {
	double x, z;
	int taken, type;
	int texture;
};

struct rocket {
	double x, z, theta;
	int texture, owner;
};
