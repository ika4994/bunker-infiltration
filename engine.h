
void changeLevel(struct player *p, struct world *w, struct enemy **e, struct node **n, struct item **i, struct rocket **r, int level);
int vecTest(double x, double z, double tx, double tz, double theta, struct world *w, int n, int b);
int wCollide(double x, double z, struct world w);
int *getPath(struct node *n, int src, int dest, int nodec,int *pathL);
