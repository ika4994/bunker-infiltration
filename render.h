#include "main.h"
#ifndef RENDER_H_INCLUDED
#define RENDER_H_INCLUDED

void render(struct textureCache t, struct player p, struct world w, struct enemy *e, struct item *i, struct rocket *r, int debug);
void drawGameMenu(struct textureCache t, int tpos, int screen, struct player p);
void initGL();

#endif // RENDER_H_INCLUDED
