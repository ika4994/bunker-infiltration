#include <math.h>
#include <windows.h>
#include <gl/gl.h>
#include <gl/wglext.h>
#include <stdio.h>
#include <al.h>
#include <alc.h>
#include "world.h"
#include "main.h"
#include "entity.h"
#include "init.h"
#include "engine.h"
#include "audio.h"

#define PIOVER180 0.0174532925

LRESULT CALLBACK WindowProc(HWND, UINT, WPARAM, LPARAM);
void EnableOpenGL(HWND hwnd, HDC*, HGLRC*);
void DisableOpenGL(HWND, HDC, HGLRC);
FILE *file;
BOOL bQuit = FALSE;
POINT p1, p2;
double diff;

int WINAPI WinMain(HINSTANCE hInstance,
                   HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine,
                   int nCmdShow)
{
    WNDCLASSEX wcex;
    HWND hwnd;
    HDC hDC;
    HGLRC hRC;
    MSG msg;
	BOOL fullscreen = FALSE;
	if (!strcmp(lpCmdLine,"-f"))
		fullscreen = TRUE;
	if (!strcmp(lpCmdLine, "f"))
		fullscreen = TRUE;
	DWORD dwExStyle;                 
	DWORD dwStyle;

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_OWNDC;
    wcex.lpfnWndProc = WindowProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = hInstance;
    wcex.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
    wcex.lpszMenuName = NULL;
    wcex.lpszClassName = "Engine-3D";
	wcex.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON));
	wcex.hIconSm = (HICON)LoadImage(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON), IMAGE_ICON, 16, 16, 0);

    if (!RegisterClassEx(&wcex))
        return 0;
	if (fullscreen){
		DEVMODE dmScreenSettings;                   // Device Mode
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));       // Makes Sure Memory's Cleared
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);       // Size Of The Devmode Structure
		dmScreenSettings.dmPelsWidth = 800;            // Selected Screen Width
		dmScreenSettings.dmPelsHeight = 600;           // Selected Screen Height
		dmScreenSettings.dmBitsPerPel = 32;             // Selected Bits Per Pixel
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;
		dwExStyle = WS_EX_APPWINDOW;                  // Window Extended Style
		dwStyle = WS_POPUP;                       // Windows Style
		ShowCursor(FALSE);                      // Hide Mouse Pointer
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);
	}
	else{
		dwExStyle = 0;
		dwStyle = NULL;
	}
    hwnd = CreateWindowEx(dwExStyle,
                          "Engine-3D",
                          "Bunker Infiltration",
                          WS_OVERLAPPED | WS_MINIMIZEBOX | WS_SYSMENU | dwStyle,
                          CW_USEDEFAULT,
						  CW_USEDEFAULT,
                          800,
                          600,
                          NULL,
                          NULL,
                          hInstance,
                          NULL);

    ShowWindow(hwnd, nCmdShow);

    EnableOpenGL(hwnd, &hDC, &hRC);

	PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = NULL;
	wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC)wglGetProcAddress("wglSwapIntervalEXT");
	wglSwapIntervalEXT(1);

    //all structures used by the engine are declared here

	//SYSTEMTIME time;
	FILETIME timef;
    struct textureCache t;
	struct audioCache a;
	struct player p;
	struct world w;
	double rads = 0;
	int menu = 1;
	int tpos = 0;
	int screen = 0;
	int mwait = 0;
	//int ct = 0, pt = 0;
	INT64 ct2, pt2, diff;
	int input;
	double esoundx, esoundz;
	int keyup = 0;

	ALCdevice* device = alcOpenDevice(NULL);
	ALCcontext* context = alcCreateContext(device, NULL);

	alcMakeContextCurrent(context);
	alcProcessContext(context);

	GetSystemTimeAsFileTime(&timef);
	//GetSystemTime(&time);

    initalizeStart(&a,&t,&p,&w);

	struct enemy *e = initalizeEnemy(&w);

	struct node *n = createGraph(&w);

	struct item *i = initalizeItems(&w, e);

	struct rocket *r = initalizeRocket(&e, &w);

	getSpawn(&p, w);

	alListener3f(AL_POSITION, 0, 0, 0);
	alListener3f(AL_VELOCITY, 0, 0, 0);
	alListener3f(AL_ORIENTATION, 0, 0, -1);

	ALuint music;

	alGenSources(1, &music);

	alGenSources(1, &p.source);

	for (int a = 0; a < w.enemyc; a++)
		alGenSources(1, &e[a].source);

    while (!bQuit)
    {
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                bQuit = TRUE;
            }
            else
            {
                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }
		else
		{
			//prev time
			GetSystemTimeAsFileTime(&timef);
			//GetSystemTime(&time);
			//pt = time.wMilliseconds;

			pt2 = ((LONGLONG)timef.dwLowDateTime + ((LONGLONG)timef.dwHighDateTime << 32LL));

			if (menu){
				alSourceStop(music);
				input = 0;
				if (GetAsyncKeyState(VK_UP) < -100)
					input = 1;
				else if (GetAsyncKeyState(VK_DOWN) < -100)
					input = 2;
				else if (GetAsyncKeyState(VK_SPACE) < -100)
					input = 3;

				
				

				updateMenu(input,&tpos,&screen,&bQuit,&menu);
				drawGameMenu(t,tpos,screen,p);


				if ((screen > 4) && input != 3) {
					keyup = 1;
				}

				
				
				if (screen == 16 && keyup == 1 && input == 3) {
					p.level = 0;
					p.lives = 3;
					p.ammo[0] = 25;
					p.ammo[1] = 0;
					p.ammo[2] = 0;
					p.ammo[3] = 0;
					p.health = 100;
					screen = 1;
					keyup = 0;
				}
				else if (screen == 15 && keyup == 1 && input == 3) {
					screen = 16;
					keyup = 0;
				}
				else if (screen == 14 && keyup == 1 && input == 3) {
					screen = 15;
					keyup = 0;
				}
				else if (p.exit == 1 && p.level == 24) {
					screen = 14;
					p.exit = 0;
				}

				if(screen == 5 && input == 3 && keyup == 1){
					p.ammo[0] = 25;
					p.ammo[1] = 0;
					p.ammo[2] = 0;
					p.ammo[3] = 0;
					changeLevel(&p, &w, &e, &n, &i,&r, p.level);
					getSpawn(&p, w);
					menu = 0;
				}
				else if (screen == 6 && input == 3 && keyup == 1) {
					if (p.level > 15)
						p.level = 16;
					else if (p.level > 6)
						p.level = 7;
					else
						p.level = 1;
					p.lives = 3;
					changeLevel(&p,&w,&e,&n,&i, &r, p.level);
					getSpawn(&p, w);
					screen = 0;
				}
				else if (screen == 7 && input == 3 && keyup == 1 && p.level == 5) {
					screen = 8;
					keyup = 0;
				}
				else if (screen == 7 && input == 3 && keyup == 1 && p.level == 11) {
					screen = 11;
					keyup = 0;
				}
				else if (screen == 7 && input == 3 && keyup == 1 && p.level == 14) {
					screen = 12;
					keyup = 0;
				}
				else if (screen == 7 && input == 3 && keyup == 1 && p.level == 23) {
					screen = 13;
					keyup = 0;
				}
				else if (screen == 7 && input == 3 && keyup == 1 && p.level == 14) {
					screen = 8;
					keyup = 0;
				}
				else if (screen == 7 && input == 3 && keyup == 1 && p.level == 6) {
					screen = 9;
					keyup = 0;
				}
				else if (screen >= 7 && input == 3 && keyup == 1) {
					printf("%d\n", p.level);
					p.level++;
					changeLevel(&p, &w, &e, &n, &i,&r, p.level);
					getSpawn(&p, w);
					p.exit = 0;
					menu = 0;
				}
				if (menu == 0) {
					switch (p.level) {
					case 1:
						alSourceStop(music);
						playSound(music, a.music1, 0, 0);
						break;
					case 2:
						alSourceStop(music);
						playSound(music, a.music2, 0, 0);
						break;
					case 3:
						alSourceStop(music);
						playSound(music, a.music3, 0, 0);
						break;
					case 4:
						alSourceStop(music);
						playSound(music, a.music4, 0, 0);
						break;
					case 5:
					case 6:
						alSourceStop(music);
						playSound(music, a.music5, 0, 0);
						break;
					case 7:
						alSourceStop(music);
						playSound(music, a.music6, 0, 0);
						break;
					case 8:
						alSourceStop(music);
						playSound(music, a.music2, 0, 0);
						break;
					case 9:
						alSourceStop(music);
						playSound(music, a.music6, 0, 0);
						break;
					case 10:
						alSourceStop(music);
						playSound(music, a.music1, 0, 0);
						break;
					case 11:
						alSourceStop(music);
						playSound(music, a.music4, 0, 0);
						break;
					case 12:
						alSourceStop(music);
						playSound(music, a.music7, 0, 0);
						break;
					case 13:
						alSourceStop(music);
						playSound(music, a.music3, 0, 0);
						break;
					case 14:
						alSourceStop(music);
						playSound(music, a.music8, 0, 0);
						break;
					case 15:
						alSourceStop(music);
						playSound(music, a.music10, 0, 0);
						break;
					case 16:
						alSourceStop(music);
						playSound(music, a.music9, 0, 0);
						break;
					case 17:
						alSourceStop(music);
						playSound(music, a.music9, 0, 0);
						break;
					case 18:
						alSourceStop(music);
						playSound(music, a.music5, 0, 0);
						break;
					case 19:
						alSourceStop(music);
						playSound(music, a.music4, 0, 0);
						break;
					case 20:
					case 21:
					case 22:
					case 23:
						alSourceStop(music);
						playSound(music, a.music10, 0, 0);
						break;
					case 24:
						alSourceStop(music);
						playSound(music, a.music9, 0, 0);
						break;
					}
				}
			}
			else{

				p.lastx = p.x;
				p.lastz = p.z;

				if (p.exit == 1) {
					menu = 1;
					screen = 7;
				}

				if (GetAsyncKeyState(VK_LCONTROL) < -100)
					p.tvel = -0.000003;
				else
					p.tvel = -0.000006;

				if (GetAsyncKeyState(VK_LEFT) < -100) //left
					p.theta += p.tvel * p.msd;
				if (GetAsyncKeyState(VK_RIGHT) < -100) //right
					p.theta -= p.tvel * p.msd;

				if (GetAsyncKeyState(VK_SPACE) < -100 && p.attack > -1)
					if (p.weapon == 0) {
						p.attack = 1;
					}
					else if (p.weapon == 1 && p.ammo[0] > 0) {
						p.attack = 1;
					}
					else if (p.weapon == 2 && p.ammo[1] > 0) {
						p.attack = 1;
					}
					else if (p.weapon == 3 && p.ammo[2] > 0) {
						p.attack = 1;
					}
					else if (p.weapon == 4 && p.ammo[3] > 0) {
						p.attack = 2;
					}
				if (GetAsyncKeyState(VK_SPACE) < -100 && p.health <= 0 && p.ypos == 0.18 && menu != 1) {
					keyup = 0;
					p.lives--;
					if (p.lives == -1) {
						menu = 1;
						screen = 6;
						playSound(p.source, a.fired, 0, 0);
					}
					else {
						menu = 1;
						screen = 5;
					}
				}

				//p.theta += ((diff/1000)*6) * p.msd;

				// get keyboard input (this has to be in the main function because of windows)
				// make sure that math.h trig functions are being used, because windows.h trig functions return integers
				p.in1 = 0;
				p.in2 = 0;
				if (GetAsyncKeyState(87) < -100){ //w
					p.in1 = 1;
				}
				if (GetAsyncKeyState(83) < -100){ //s
					if (!p.in1)
						p.in1 = 2;
					else
						p.in2 = 2;
				}
				if (GetAsyncKeyState(65) < -100){ //a
					if (!p.in1)
						p.in1 = 3;
					else if (!p.in2)
						p.in2 = 3;
				}
				if (GetAsyncKeyState(68) < -100){ //d
					if (!p.in1)
						p.in1 = 4;
					else if (!p.in2)
						p.in2 = 4;
				}

				if (GetAsyncKeyState(VK_LSHIFT) < -100)
					p.vel = 0.0000001 * p.msd;
				else
					p.vel = 0.00000015 * p.msd;

				//weapon selection
				if (GetAsyncKeyState(48) < -100) {
					p.weapon = 0;
					alSourceStop(p.source);
				}
				else if (GetAsyncKeyState(49) < -100 && p.ammo[0] > 0) {
					p.weapon = 1;
					alSourceStop(p.source);
				}
				else if (GetAsyncKeyState(50) < -100 && p.ammo[1] > 0) {
					p.weapon = 2;
					alSourceStop(p.source);
				}
				else if (GetAsyncKeyState(51) < -100 && p.ammo[2] > 0) {
					p.weapon = 3;
					alSourceStop(p.source);
				}
				else if (GetAsyncKeyState(52) < -100 && p.ammo[3] > 0) {
					p.weapon = 4;
					alSourceStop(p.source);
				}
				//audio

				alListener3f(AL_POSITION, 0, 0, 0);
				alListener3f(AL_VELOCITY, 0, 0, 0);
				alListener3f(AL_DIRECTION, 0, 0, 0);

				if (p.attack == 1) {
					switch (p.weapon) {
					case 1:
						playSound(p.source, a.gun1, 0, 0);
						break;
					case 2:
						playSound(p.source, a.gun2, 0, 0);
						break;
					case 3:
						playSound(p.source, a.gun3, 0, 0);
						break;
					case 4:
						playSound(p.source, a.gun4, 0, 0);
						break;
					}
				}

				for (int b = 0; b < w.enemyc; b++) {

					esoundx = (p.x - (-e[b].x)) * cos(-p.theta*PIOVER180) - (p.z - (-e[b].z))*sin(-p.theta*PIOVER180);
					esoundz = (p.x - (-e[b].x)) * sin(-p.theta*PIOVER180) + (p.z - (-e[b].z))*cos(-p.theta*PIOVER180);

					if (e[b].attack == 1) {
						switch (e[b].weapon) {
						case 1:
							playSound(e[b].source, a.gun1, esoundx , esoundz);
							break;
						case 2:
							playSound(e[b].source, a.gun2, esoundx, esoundz);
							break;
						case 3:
						case 5:
						case 7:
							playSound(e[b].source, a.gun3, esoundx, esoundz);
							break;
						case 4:
						case 6:
							playSound(e[b].source, a.gun4, esoundx, esoundz);
							break;
						}
					}
				}

				update(a, &t, &p, &e, &n, &r, &w, &i);

				
			}

			//wglSwapIntervalEXT(1);
			//wglGetSwapIntervalEXT();
            SwapBuffers(hDC);

			//calculate framerate
			
			if (p.msd > 0)
				p.fps = 10000000 /(p.msd);
			if (p.fps > 999999)
				p.fps = 999999;

			//get frame time

			GetSystemTimeAsFileTime(&timef);
			//GetSystemTime(&time);
			//ct = time.wMilliseconds;
			ct2 = ((LONGLONG)timef.dwLowDateTime + ((LONGLONG)timef.dwHighDateTime << 32LL));
			p.msd = ct2 - pt2;
			diff = ct2 - pt2;
			//printf("FILETIME NSD : %d\n", diff);

			if (p.msd <= 0)
				p.msd = 1;

        }
    }
	//fclose(file);
	alDeleteSources(1, music);
	for (int a = 0; a < w.enemyc; a++) {
		if(e[a].pathMEM == 1)
			free(e[a].path);
	}
	free(i);
	free(p.ammo);
	freeGraph(n, w);
	alDeleteSources(1, &p.source);
	for (int a = 0; a < w.enemyc; a++)
		alDeleteSources(1, &e[a].source);
	free(e);
	free(r);
	freeWorld(w);
	alcDestroyContext(context);
	alcCloseDevice(device);
    DisableOpenGL(hwnd, hDC, hRC);
    DestroyWindow(hwnd);
    return msg.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
        case WM_CLOSE:
			//fclose(file);
			bQuit = TRUE;
        break;

        case WM_DESTROY:
			bQuit = TRUE;
			break;

        case WM_KEYDOWN:
        {
            switch (wParam)
            {
                case VK_ESCAPE:
					//fclose(file);
					bQuit = TRUE;
                break;
            }
        }
        break;

		case WM_MOUSEMOVE:

			break;

        default:
            return DefWindowProc(hwnd, uMsg, wParam, lParam);
    }

    return 0;
}

void EnableOpenGL(HWND hwnd, HDC* hDC, HGLRC* hRC)
{
    PIXELFORMATDESCRIPTOR pfd;

    int iFormat;

    *hDC = GetDC(hwnd);

    ZeroMemory(&pfd, sizeof(pfd));

    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW |
                  PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 16;
    pfd.iLayerType = PFD_MAIN_PLANE;

    iFormat = ChoosePixelFormat(*hDC, &pfd);

    SetPixelFormat(*hDC, iFormat, &pfd);

    *hRC = wglCreateContext(*hDC);

    wglMakeCurrent(*hDC, *hRC);
}

void DisableOpenGL (HWND hwnd, HDC hDC, HGLRC hRC)
{
    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hRC);
    ReleaseDC(hwnd, hDC);
}

        