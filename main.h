#include <stdio.h>
#include <Windows.h>
#include <gl/gl.h>
#include <al.h>

#ifndef MAIN_H_INCLUDED
#define MAIN_H_INCLUDED

#define IDI_ICON 101

//the texture cache stores all the current loaded textures in the game
struct textureCache{
    GLuint font;
	GLuint shader;

	GLuint oil;
	GLuint health;
	GLuint armor;

	GLuint brick1;
	GLuint brick2;
	GLuint brick3;
	GLuint brick4;
	GLuint brick5;
	GLuint brick6;
	GLuint brick7;
	GLuint brick8;
	GLuint brick9;
	GLuint brick10;

	GLuint metal1;
	GLuint metal2;
	GLuint metal3;
	GLuint metal4;
	GLuint metal5;
	GLuint metal6;
	GLuint metal7;
	GLuint metal8;
	GLuint metal9;

	GLuint guts1;
	GLuint guts2;
	GLuint guts3;

	GLuint rock1;
	GLuint rock2;

	GLuint trump1;
	GLuint trump2;
	GLuint trump3;
	GLuint trump4;
	GLuint trump5;
	GLuint trump6;

	GLuint post1;
	GLuint post2;
	GLuint post3;
	GLuint post4;

	GLuint title;
	GLuint title2;

	GLuint story1;
	GLuint story2;
	GLuint story3;
	GLuint story4;
	GLuint story5;
	GLuint story6;
	GLuint story7;

	GLuint death1;
	GLuint death2;

	GLuint xhair;

	GLuint gun0_1;
	GLuint gun0_2;
	GLuint gun1_1;
	GLuint gun1_2;
	GLuint gun1_3;
	GLuint gun2_1;
	GLuint gun2_2;
	GLuint gun2_3;
	GLuint gun2_4;
	GLuint gun3_1;
	GLuint gun3_2;
	GLuint gun3_3;
	GLuint gun4_1;

	GLuint gun1_w;
	GLuint gun2_w;
	GLuint gun3_w;
	GLuint gun4_w;

	GLuint enemy_1_1;
	GLuint enemy_1_2;
	GLuint enemy_2_1;
	GLuint enemy_2_2;
	GLuint enemy_3_1;
	GLuint enemy_3_2;
	GLuint enemy_3_3;
	GLuint enemy_4_1;
	GLuint enemy_5_1;
	GLuint enemy_6_1;
	GLuint enemy_6_2;
	GLuint enemy_7_1;
	GLuint enemy_7_2;
	GLuint enemy_7_3;

	GLuint time1;
	GLuint time2;

	GLuint intermission;

	GLuint rocket_1;
	GLuint rocket_2;

	GLuint sky1;

	GLuint end1;
	GLuint end2;
	GLuint end3;
};

struct audioCache {
	ALuint aa1;

	ALuint gun1,gun2,gun3,gun4;

	ALuint fired;

	ALuint music1;
	ALuint music2;
	ALuint music3;
	ALuint music4;
	ALuint music5;
	ALuint music6;
	ALuint music7;
	ALuint music8;
	ALuint music9;
	ALuint music10;
};

void update(struct audioCache audio, struct textureCache *t, struct player *p, struct enemy **e, struct node **n, struct rocket **r, struct world *w, struct item **i);
void updateMenu(int input, int *tpos, int *screen, BOOL *bQuit, int *menu);

#endif // MAIN_H_INCLUDED
