#include <Windows.h>
#include <GL/glew.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <stdio.h>
#include <math.h>
#include "main.h"
#include "world.h"
#include "render.h"
#include "entity.h"

#define DEATHVEL 0.00036

//this file contains all resources related to drawing graphics

//draw a background image
void drawBackground(GLuint texture) {
	glDisable(GL_DEPTH_TEST);
	glBindTexture(GL_TEXTURE_2D, texture);
	glPushMatrix();
	glTranslatef(0, 0, -1.7);
	glBegin(GL_POLYGON);
	glTexCoord2d(0, 0); glVertex3f(-1, -1, 0.0);
	glTexCoord2d(1, 0); glVertex3f(1, -1, 0.0);
	glTexCoord2d(1, 1); glVertex3f(1, 1, 0.0);
	glTexCoord2d(0, 1); glVertex3f(-1, 1, 0.0);
	glEnd();
	glPopMatrix();
	glEnable(GL_DEPTH_TEST);
}


//draw a 2d sprite
void drawSprite(GLuint texture, int w,int h,float x, float y, float z, float theta,float d) {

	float glw = w / 16 * 0.025;
	float glh = h / 16 * 0.025;

	glBindTexture(GL_TEXTURE_2D, texture);
	glPushMatrix();
	
	glTranslatef(x, d, z);
	glTranslatef(0, 0, 0);
	glRotatef(-theta, 0, 1, 0);

	glBegin(GL_POLYGON);

	glTexCoord2d(1, 0); glVertex3f(-(glw / 2), -(glh / 2) + y, 0.0);
	glTexCoord2d(0, 0); glVertex3f((glw / 2), -(glh / 2) + y, 0.0);
	glTexCoord2d(0, 1); glVertex3f((glw / 2), (glh / 2) + y, 0.0);
	glTexCoord2d(1, 1); glVertex3f(-(glw / 2), (glh / 2) + y, 0.0);

	glEnd();
	glPopMatrix();
}

//draw a textured cube on the screen
void drawCube(GLuint texture, float x, float y, float z, float theta, float size){
	
	GLfloat verts[8][3] = {
	{ size, size, size }, //0
	{ -size, size, size }, //1
	{ size, -size, size }, //2
	{ -size, -size, size }, //3
	{ size, size, -size }, //4
	{ -size, size, -size }, //5
	{ size, -size, -size }, //6
	{ -size, -size, -size }  //7
	};
	
	glBindTexture(GL_TEXTURE_2D, texture);

	glPushMatrix();

	glTranslated(x,y,z);
	//glRotatef(theta, 0, 1, 0);
	glBegin(GL_QUADS);
	
	//front
	glTexCoord2d(1, 1); glVertex3fv(verts[0]);
	glTexCoord2d(1, 0); glVertex3fv(verts[2]);
	glTexCoord2d(0, 0); glVertex3fv(verts[3]);
	glTexCoord2d(0, 1); glVertex3fv(verts[1]);

	//right
	glTexCoord2d(0, 1); glVertex3fv(verts[0]); //lower left
	glTexCoord2d(1, 1); glVertex3fv(verts[4]); //lower right
	glTexCoord2d(1, 0); glVertex3fv(verts[6]); //upper left
	glTexCoord2d(0, 0); glVertex3fv(verts[2]); //upper right
	
	//left
	glTexCoord2d(1, 0); glVertex3fv(verts[3]);
	glTexCoord2d(0, 0); glVertex3fv(verts[7]);
	glTexCoord2d(0, 1); glVertex3fv(verts[5]);
	glTexCoord2d(1, 1); glVertex3fv(verts[1]);

	glTexCoord2d(0, 1); glVertex3fv(verts[4]);
	glTexCoord2d(0, 0); glVertex3fv(verts[6]);
	glTexCoord2d(1, 0); glVertex3fv(verts[7]);
	glTexCoord2d(1, 1); glVertex3fv(verts[5]);

	glEnd();
	glPopMatrix();
}

//draw all rockets
void drawRockets(struct rocket *r, struct textureCache t, float x, float y, float z, float theta, int rocketc) {

	glPushMatrix();

	glRotatef(theta, 0, 1, 0);
	glTranslatef(x, 0, z);

	for (int a = 0; a < rocketc; a++) {
			switch (r[a].texture) {
			case 1:
				drawSprite(t.rocket_1, 96, 96, -r[a].x, y, -r[a].z, theta, 0);
				break;
			case 2:
				drawSprite(t.rocket_2, 256, 256, -r[a].x, y, -r[a].z, theta, 0);
				break;
			}
	}

	glPopMatrix();
}

//draw all items
void drawItems(struct item *i, struct textureCache t, float x, float y, float z, float theta, int itemc) {

	glPushMatrix();

	glRotatef(theta, 0, 1, 0);
	glTranslatef(x, 0, z);

	for (int a = 0; a < itemc; a++) {
		if (i[a].taken == 0) {
			switch (i[a].type) {
			case 0:
				drawSprite(t.oil, 128, 256, -i[a].x, y, -i[a].z, theta, -0.1);
				break;
			case 1:
				drawSprite(t.gun1_w, 96, 64, -i[a].x, y, -i[a].z, theta, -0.22);
				break;
			case 2:
				drawSprite(t.gun2_w, 160, 96, -i[a].x, y, -i[a].z, theta, -0.22);
				break;
			case 3:
				drawSprite(t.gun3_w, 160, 96, -i[a].x, y, -i[a].z, theta, -0.22);
				break;
			case 4:
				drawSprite(t.health, 64, 96, -i[a].x, y, -i[a].z, theta, -0.22);
				break;
			case 5:
				drawSprite(t.armor, 128, 128, -i[a].x, y, -i[a].z, theta, -0.22);
				break;
			case 6:
				drawSprite(t.gun4_w, 160, 96, -i[a].x, y, -i[a].z, theta, -0.22);
				break;
			}
		}
	}

	glPopMatrix();
}

//draw all enemies
void drawEnemies(struct enemy *e, struct textureCache t , float x, float y, float z, float theta, int enemyc) {

	glPushMatrix();
	
	glRotatef(theta, 0, 1, 0);
	glTranslatef(x, 0, z);

	for (int a = 0; a < enemyc; a++) {
		if (e[a].mode != 4) {
			switch (e[a].weapon) {
			case 1:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_1_1, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_1_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			case 2:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_2_1, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_2_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			case 3:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_3_1, 170, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_3_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 2:
					drawSprite(t.enemy_3_3, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			case 4:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_4_1, 120, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_3_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			case 5:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_5_1, 170, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_3_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 2:
					drawSprite(t.enemy_3_3, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			case 6:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_6_1, 120, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_6_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			case 7:
				switch (e[a].texture) {
				case 0:
					drawSprite(t.enemy_7_1, 120, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 1:
					drawSprite(t.enemy_7_2, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				case 2:
					drawSprite(t.enemy_7_3, 128, 256, e[a].x, y, e[a].z, theta, -0.05);
					break;
				}
				break;
			}
		}
	}
	glPopMatrix();
}

//draw the world and orient it around the camera
int drawWorld(struct textureCache t, struct world w, float x, float y, float z, float theta){

	int blocks = 0;

	glPushMatrix();

	glRotatef(theta, 0, 1, 0);
	glTranslatef(x,0, z);

	for (int a = 0; a < w.w; a++)
		for (int b = 0; b < w.h; b++){
			switch (w.tile[a][b]){
			case 1: //stone brick block
				blocks++;	
				drawCube(t.brick1, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 2: //large brick block
				blocks++;
				drawCube(t.brick2, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 3: //red brick block
				blocks++;
				drawCube(t.brick3, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 4: //vine brick block
				blocks++;
				drawCube(t.brick4, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 5: //spray brick block
				blocks++;
				drawCube(t.brick5, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 6: 
				blocks++;
				drawCube(t.brick6, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 7:
				blocks++;
				drawCube(t.brick7, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 8:
				blocks++;
				drawCube(t.brick8, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 9:
				blocks++;
				drawCube(t.brick9, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 10: //metal wall
				blocks++;
				drawCube(t.metal1, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 11: //tiled metal
				blocks++;
				drawCube(t.metal2, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 12: // computer monitor
				blocks++;
				drawCube(t.metal3, -(a * 0.5), y,-(b * 0.5), 0, 0.25);
				break;
			case 13:
				blocks++;
				drawCube(t.metal4, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 14:
				blocks++;
				drawCube(t.metal5, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 15:
				blocks++;
				drawCube(t.metal6, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 16:
				blocks++;
				drawCube(t.metal7, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 17:
				blocks++;
				drawCube(t.metal8, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 18:
				blocks++;
				drawCube(t.metal9, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 19:
				blocks++;
				drawCube(t.brick10, - (a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 20: //rock
				blocks++;
				drawCube(t.rock1, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 21: //rock with runes
				blocks++;
				drawCube(t.rock2, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 30: //guts
				blocks++;
				drawCube(t.guts1, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 31: //baloo's guts
				blocks++;
				drawCube(t.guts2, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 32: 
				blocks++;
				drawCube(t.guts3, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 33:
				blocks++;
				drawCube(t.time1, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 34:
				blocks++;
				drawCube(t.time2, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 40: //trump face
				blocks++;
				drawCube(t.trump1, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 41: //trump poster 1
				blocks++;
				drawCube(t.trump2, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 42: //trump poster 2
				blocks++;
				drawCube(t.trump3, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 43: //trump poster 3
				blocks++;
				drawCube(t.trump4, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 44: //trump poster 4
				blocks++;
				drawCube(t.trump5, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 45: //trump danger
				blocks++;
				drawCube(t.trump6, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 50: //poster
				blocks++;
				drawCube(t.post1, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 51: //poster
				blocks++;
				drawCube(t.post2, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 52: //poster
				blocks++;
				drawCube(t.post3, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			case 53: //poster
				blocks++;
				drawCube(t.post4, -(a * 0.5), y, -(b * 0.5), 0, 0.25);
				break;
			}
		}

	glPopMatrix();



	return blocks;
}

//draw a textured square on the screen
void drawSquare(GLuint texture,int x,int y,int w,int h,float tx1,float ty1,float tx2,float ty2){

    //convert pixel coordinates into openGL coordinates

    float glx = x * 0.0025;
    float gly = y * 0.003333;
    float glw = w/16 * 0.025;
    float glh = h/16 * 0.025;

	
    
    glBindTexture(GL_TEXTURE_2D, texture);
    glPushMatrix();
    glTranslatef(glx,gly,-2);
    glBegin(GL_POLYGON);
    glTexCoord2d(tx1,ty1); glVertex3f (-(glw/2), -(glh/2), 0.0);
    glTexCoord2d(tx2,ty1); glVertex3f ((glw/2), -(glh/2), 0.0);
    glTexCoord2d(tx2,ty2); glVertex3f ((glw/2), (glh/2), 0.0);
    glTexCoord2d(tx1,ty2); glVertex3f (-(glw/2), (glh/2), 0.0);
    glEnd();
    glPopMatrix();
}

//draw a letter on the screen
void drawChar(GLuint texture,char c,int x,int y,int size){

    //declarations
    float tx1=0,ty1=0,tx2=0,ty2=0;

	//make sure letter is uppercase
	if (c > 97)
		c -= 32;

    //convert to a number between 0 and 25
    int letter = c - 65;

    //set up the texture so that it only displays the corresponding letter
    tx1 = 0.166666 * (letter % 6);
    tx2 = 0.166666 * ((letter % 6) + 1);


    if(letter < 6){
        ty2 = 0.166666 * 6;
        ty1 = 0.166666 * 5;
    }
    else if(letter < 12){
        ty2 = 0.166666 * 5;
        ty1 = 0.166666 * 4;
    }
    else if(letter < 18){
        ty2 = 0.166666 * 4;
        ty1 = 0.166666 * 3;
    }
    else if(letter < 24){
        ty2 = 0.166666 * 3;
        ty1 = 0.166666 * 2;
    }
    else{
        ty2 = 0.166666 * 2;
        ty1 = 0.166666 * 1;
    }

    //draw the texture
    drawSquare(texture,x,y,size,size,tx1,ty1,tx2,ty2);
}

//draw a number 0-9 on the screen
void drawNumber(GLuint texture,int n,int x,int y,int size){

    //declarations
    float tx1=0,ty1=0,tx2=0,ty2=0;

    int offsetx=0,offsety=0;

    //set up the texture so that it only displays the corresponding number
    switch(n)
    {
    case 0:
        offsetx=5;
        offsety=0;
        break;
    case 1:
        offsetx=2;
        offsety=1;
        break;
    case 2:
        offsetx=3;
        offsety=1;
        break;
    case 3:
        offsetx=4;
        offsety=1;
        break;
    case 4:
        offsetx=5;
        offsety=1;
        break;
    case 5:
        offsetx=0;
        offsety=0;
        break;
    case 6:
        offsetx=1;
        offsety=0;
        break;
    case 7:
        offsetx=2;
        offsety=0;
        break;
    case 8:
        offsetx=3;
        offsety=0;
        break;
    case 9:
        offsetx=4;
        offsety=0;
        break;
    default:
        offsetx=5;
        offsety=0;
        break;
    }

    tx1 = 0.166666 * offsetx;
    tx2 = 0.166666 * (offsetx + 1);
    ty1 = 0.166666 * offsety;
    ty2 = 0.166666 * (offsety + 1);

    drawSquare(texture,x,y,size,size,tx1,ty1,tx2,ty2);
}

//draw an integer 0 - 999
void drawInt(GLuint texture, int n, int x, int y, int size){
	int num[3] = { 0, 0, 0 };
	num[2] = (n % 100) % 10;
	num[1] = ((n % 100) - num[2]) / 10;
	num[0] = (n - (num[1] * 10) - num[2]) / 100;
	for (int a = 0; a < 3; a++)
		drawNumber(texture, num[a], x + a*((size / 2) + (size / 8)), y, size);
}

//draw an integer 0-999999
void drawIntLG(GLuint texture, int n, int x, int y, int size) {
	int num[6] = { 0, 0, 0, 0, 0, 0 };
	num[5] = n % 10;
	num[4] = ((n % 100) - (n % 10)) / 10;
	num[3] = ((n % 1000) - (n % 100)) / 100;
	num[2] = ((n % 10000) - (n % 1000)) / 1000;
	num[1] = ((n % 100000) - (n % 10000)) / 10000;
	num[0] = ((n % 1000000) - (n % 100000)) / 100000;
	for (int a = 0; a < 6; a++)
		drawNumber(texture, num[a], x + a*((size / 2) + (size / 8)), y, size);
}

//draw a float value
void drawFloat(GLuint texture, float n, int x, int y, int size){
	int temp = truncf(n);
	drawInt(texture, temp, x, y, size);
	double temp2 = n - temp;
	int num[3] = { 0, 0, 0};
	num[0] = truncf(temp2 * 10);
	num[1] = truncf(temp2 * 100);
	num[2] = truncf(temp2 * 1000);
	num[1] %= 10;
	num[2] %= 100;
	num[2] %= 10;
	for (int a = 0; a < 3; a++)
		drawNumber(texture, num[a], x + (a+4)*((size / 2) + (size / 8)), y, size);
}

//draw a string of letters, numbers, and spaces on the screen
void drawString(GLuint texture,char *string,int x,int y,int size){
    int a;
    for(a=0;string[a] != '\0';a++){
        if(string[a] >= 48 && string[a] <= 57)
            drawNumber(texture,string[a]-48,x+a*((size/2)+(size/8)),y,size);
        else if(string[a] >= 65 && string[a] <= 90)
            drawChar(texture,string[a],x+a*((size/2)+(size/8)),y,size);
    }
}

//draw the HUD
void drawHUD(GLuint font,int health,int armor,int weapon,int *ammo,int lives,int score){
	drawString(font, "SCORE", -380, 280, 48);
	drawString(font, "PLAYER X", 80, 280, 48);
	drawString(font, "HEALTH", -380, -240, 48);
	drawString(font, "ARMOR", -380, -280, 48);
	switch (weapon){	
	case -1:
		drawString(font, "UNARMED", 220, -240, 48);
		break;
	case 0:
		drawString(font, "KNIFE", 220, -240, 48);
		break;
	case 1:
		drawString(font, "HANDGUN", 220, -240, 48);
		drawString(font, "AMMO", 220, -280, 48);
		drawInt(font,ammo[0], 340, -280, 48);
		break;
	case 2:
		drawString(font, "SHOTGUN", 220, -240, 48);
		drawString(font, "AMMO", 220, -280, 48);
		drawInt(font, ammo[1], 340, -280, 48);
		break;
	case 3:
		drawString(font, "ASSAULT", 220, -240, 48);
		drawString(font, "AMMO", 220, -280, 48);
		drawInt(font, ammo[2], 340, -280, 48);
		break;
	case 4:
		drawString(font, "ROCKET", 220, -240, 48);
		drawString(font, "AMMO", 220, -280, 48);
		drawInt(font, ammo[3], 340, -280, 48);
		break;
	}
	drawIntLG(font, score, -200, 280, 48);
	drawInt(font, lives, 340, 280, 48);
	drawInt(font, health, -150, -240, 48);
	drawInt(font, armor, -150, -280, 48);
}

//draw the weapon texture
void drawGun(struct textureCache t, int weapon, int texture) {
	//draw crosshair
	drawSquare(t.xhair, 0, 0, 32, 32, 0, 0, 1, 1);

	switch (weapon) {
	case 0:
		switch (texture) {
		case 0:
			drawSquare(t.gun0_1, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 1:
			drawSquare(t.gun0_2, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		}
		break;
	case 1:
		switch (texture) {
		case 0:
			drawSquare(t.gun1_1, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 1:
			drawSquare(t.gun1_2, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 2:
			drawSquare(t.gun1_3, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		}
		break;
	case 2:
		switch (texture) {
		case 0:
			drawSquare(t.gun2_1, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 1:
			drawSquare(t.gun2_2, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 2:
			drawSquare(t.gun2_3, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 3:
			drawSquare(t.gun2_4, 20, -170, 768, 768, 0, 0, 1, 1);
			break;
		}
		break;
	case 3:
		switch (texture) {
		case 0:
			drawSquare(t.gun3_1, 140, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 1:
			drawSquare(t.gun3_2, 140, -170, 768, 768, 0, 0, 1, 1);
			break;
		case 2:
			drawSquare(t.gun3_3, 140, -170, 768, 768, 0, 0, 1, 1);
			break;
		}
		break;
	case 4:
		drawSquare(t.gun4_1, 140, -170, 768, 768, 0, 0, 1, 1);
		break;
	}
}

//draw the debug information
void drawDebug(struct textureCache t,struct player p, struct world w,int blocks){

	drawString(t.font, "DEBUG INFORMATION", -400, 220, 32);

	drawString(t.font, "PLAYERX", -400, 180, 32);
	drawString(t.font, "PLAYERZ", -400, 140, 32);
	drawString(t.font, "PLAYERT", -400, 100, 32);
	drawString(t.font, "RMANGLE", -400, 60, 32);
	drawString(t.font, "BLOCKS", -400, 20, 32);
	drawString(t.font, "FRAMETIME", -400, -20, 32);
	drawString(t.font, "FPS", -400, -60, 32);
	drawString(t.font, "ENEMYC", -400, -100, 32);

	drawFloat(t.font, p.x, -240, 180, 32);
	drawFloat(t.font, p.z, -240, 140, 32);
	drawFloat(t.font, p.theta, -240, 100, 32);
	drawFloat(t.font, p.ma, -240, 60, 32);
	drawInt(t.font, blocks, -240, 20, 32);
	drawIntLG(t.font, p.msd, -200, -20, 32);
	drawIntLG(t.font, p.fps, -240, -60, 32);
	drawInt(t.font, w.enemyc, -240, -100, 32);

}

//main rendering function
void render(struct textureCache t,struct player p,struct world w,struct enemy *e,struct item *i,struct rocket *r,int debug){

	int blocks = 0;

    //clear the screen
	if(p.level == 24)
		glClearColor(1, 0, 0, 0.0f);
	else if(p.level > 22)
		glClearColor(1, 0, 0.25, 0.0f);
	else if (p.level == 12 || p.level == 20 || p.level == 21)
		glClearColor(0.1f, 0.0f, 0.1f, 0.0f);
	else if(p.level > 16 || p.level == 22)
		glClearColor(0.52, 0.37, 0.26, 0.0f);
	else if(p.level > 6)
		glClearColor(0.1, 0.01, 0.01, 0.0f);
	else
		glClearColor(0.35f, 0.35f, 0.35f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (p.level == 16 || p.level == 18) {
		drawBackground(t.sky1);
	}

	drawRockets(r, t, p.x, p.ypos, p.z, p.theta, w.rocketc);
	drawEnemies(e, t, p.x, p.ypos, p.z, p.theta,w.enemyc);
	drawItems(i, t, p.x, p.ypos, p.z, p.theta, w.itemc);
	blocks = drawWorld(t, w, p.x, p.ypos, p.z, p.theta);

	//disable z-buffer for all HUD elemtents
	glDisable(GL_DEPTH_TEST);
	drawGun(t, p.weapon, p.guntexture);
	drawHUD(t.font, p.health, p.armor,p.weapon,p.ammo,p.lives,p.score);
	if (debug){
		drawDebug(t, p, w, blocks);
	}
	glEnable(GL_DEPTH_TEST);

}


void drawGameMenu(struct textureCache t,int tpos,int screen,struct player p){

	GLuint texture;

	if (screen == 5)
		texture = t.death1;
	else if( screen == 11)
		texture = t.story5;
	else if (screen == 12)
		texture = t.story6;
	else if (screen == 13)
		texture = t.story7;
	else if (screen == 14)
		texture = t.end1;
	else if (screen == 15)
		texture = t.end2;
	else if (screen == 16)
		texture = t.end3;
	else if (screen == 6)
		texture = t.death2;
	else if (screen == 7)
		texture = t.intermission;
	else if (screen == 8)
		texture = t.story2;
	else if (screen == 9)
		texture = t.story3;
	else if (screen == 10)
		texture = t.story4;
	else if (screen > 0 && screen != 3)
		texture = t.title2;
	else if (screen == 3)
		texture = t.story1;
	else
		texture = t.title;

	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, texture);
	glPushMatrix();
	glTranslatef(0, 0, -1.7);
	glBegin(GL_POLYGON);
	glTexCoord2d(0, 0); glVertex3f(-1, -1, 0.0);
	glTexCoord2d(1, 0); glVertex3f(1, -1, 0.0);
	glTexCoord2d(1, 1); glVertex3f(1, 1, 0.0);
	glTexCoord2d(0, 1); glVertex3f(-1, 1, 0.0);
	glEnd();
	glPopMatrix();

	glPushMatrix();
	switch (tpos){
	case 0:
		glTranslatef(-0.92, -0.5, -1.7);
		break;
	case 1:
		glTranslatef(-0.92, -0.62, -1.7);
		break;
	case 2:
		glTranslatef(-0.92, -0.74, -1.7);
		break;
	case 3:
		glTranslatef(-0.92, -0.86, -1.7);
		break;
	}
	if (screen < 3){
		glRotatef(270, 0, 0, 1);
		glBegin(GL_TRIANGLES);
		glVertex3f(0.0f, 0.03f, 0.0f);
		glVertex3f(-0.03f, -0.03f, 0.0f);
		glVertex3f(0.03f, -0.03f, 0.0f);
		glEnd();
	}

	glPopMatrix();

	glDisable(GL_DEPTH_TEST);

	switch (screen){
	case 0:
		drawString(t.font, "PLAY", -390, -180, 48);
		drawString(t.font, "CREDITS", -390, -220, 48);
		drawString(t.font, "INSTRUCTIONS", -390, -260, 48);
		drawString(t.font, "QUIT", -390, -300, 48);
		break;
	case 1:
		drawString(t.font, "BACK", -390, -220, 48);
		drawString(t.font, "CREDITS", -390, 300, 64);
		drawString(t.font, "PROGRAMMING BY", -390, 240, 64);
		drawString(t.font, "JOHN", -390, 200, 48);
		drawString(t.font, "TEXTURES BY", -390, 160, 64);
		drawString(t.font, "FABIAN", -390, 120, 48);
		drawString(t.font, "JOHN", -390, 80, 48);
		drawString(t.font, "BALOO", -390, 40, 48);
		drawString(t.font, "ZIZOU", -390, 0, 48);
		drawString(t.font, "ZAK", -390, -40, 48);
		drawString(t.font, "EMILJIANO", -390, -80, 48);
		break;
	case 4:
		drawString(t.font, "HOW TO PLAY", -390, 300, 64);
		drawString(t.font, "CONTROLS", -390, 240, 64);
		drawString(t.font, "W TO MOVE FORWARD", -390, 200, 48);
		drawString(t.font, "A TO STRAFE LEFT", -390, 160, 48);
		drawString(t.font, "S TO MOVE BACKWARD", -390, 120, 48);
		drawString(t.font, "D TO STRAFE RIGHT", -390, 80, 48);
		drawString(t.font, "0 TO EQUIP KNIFE", -390, 40, 48);
		drawString(t.font, "1 TO EQUIP HANDGUN", -390, 0, 48);
		drawString(t.font, "2 TO EQUIP SHOTGUN", -390, -40, 48);
		drawString(t.font, "3 TO EQUIP ASSAULT RIFLE", -390, -80, 48);
		drawString(t.font, "SHIFT TO WALK", -390, -120, 48);
		drawString(t.font, "CTRL TO SLOW TURN SPEED", -390, -160, 48);
		break;
	case 5:
		drawString(t.font, "PLAYER X", -400, 0, 48);
		drawString(t.font, "PRESS SPACE", -400, -60, 48);
		drawInt(t.font, p.lives, -140, 0, 48);
		break;
	case 7:
		//printf("%d\n", p.level);
		drawString(t.font, "NOW EXITING", -380, 280, 48);
		drawString(t.font, "NEXT UP", -380, -240, 48);
		switch (p.level) {
		case 1:
			drawString(t.font, "BUNKER ENTRANCE", -380, 240, 64);
			drawString(t.font, "OIL STORAGE", -380, -280, 64);
			break;
		case 2:
			drawString(t.font, "OIL STORAGE", -380, 240, 64);
			drawString(t.font, "BARRACKS", -380, -280, 64);
			break;
		case 3:
			drawString(t.font, "BARRACKS", -380, 240, 64);
			drawString(t.font, "COMPUTER COMPLEX", -380, -280, 64);
			break;
		case 4:
			drawString(t.font, "COMPUTER COMPLEX", -380, 240, 64);
			drawString(t.font, "COMMAND CENTER", -380, -280, 64);
			break;
		case 5:
			drawString(t.font, "COMMAND CENTER", -380, 240, 64);
			drawString(t.font, "JIHADI JOHNS LAIR", -380, -280, 64);
			break;
		case 6:
			drawString(t.font, "JIHADI JOHNS LAIR", -380, 240, 64);
			drawString(t.font, "CAVE ENTRANCE", -380, -280, 64);
			break;
		case 7:
			drawString(t.font, "CAVE ENTRANCE", -380, 240, 64);
			drawString(t.font, "OIL RESERVES", -380, -280, 64);
			break;
		case 8:
			drawString(t.font, "OIL RESERVES", -380, 240, 64);
			drawString(t.font, "AMMUNITION STORAGE", -380, -280, 64);
			break;
		case 9:
			drawString(t.font, "AMMUNITION STORAGE", -380, 240, 64);
			drawString(t.font, "BARRACKS 2", -380, -280, 64);
			break;
		case 10:
			drawString(t.font, "BARRACKS 2", -380, 240, 64);
			drawString(t.font, "COMMAND CENTER 2", -380, -280, 64);
			break;
		case 11:
			drawString(t.font, "COMMAND CENTER 2", -380, 240, 64);
			drawString(t.font, "SPACE TIME WARP", -380, -280, 64);
			break;
		case 12:
			drawString(t.font, "SPACE TIME WARP", -380, 240, 64);
			drawString(t.font, "SECRET TUNNELS", -380, -280, 64);
			break;
		case 13:
			drawString(t.font, "SECRET TUNNELS", -380, 240, 64);
			drawString(t.font, "KHATTABS CAVE", -380, -280, 64);
			break;
		case 14:
			drawString(t.font, "KHATTABS CAVE", -380, 240, 64);
			drawString(t.font, "SPACE TIME WARP 2", -380, -280, 64);
			break;
		case 15:
			drawString(t.font, "SPACE TIME WARP 2", -380, 240, 64);
			drawString(t.font, "DESERT", -380, -280, 64);
			break;
		case 16:
			drawString(t.font, "DESERT", -380, 240, 64);
			drawString(t.font, "MILITARY BASE", -380, -280, 64);
			break;
		case 17:
			drawString(t.font, "MILITARY BASE", -380, 240, 64);
			drawString(t.font, "DESERT 2", -380, -280, 64);
			break;
		case 18:
			drawString(t.font, "DESERT 2", -380, 240, 64);
			drawString(t.font, "COMPUTER COMPLEX 2", -380, -280, 64);
			break;
		case 19:
			drawString(t.font, "COMPUTER COMPLEX 2", -380, 240, 64);
			drawString(t.font, "THE ENTRANCE", -380, -280, 64);
			break;
		case 20:
			drawString(t.font, "THE ENTRANCE", -380, 240, 64);
			drawString(t.font, "THE INSIDES", -380, -280, 64);
			break;
		case 21:
			drawString(t.font, "THE INSIDES", -380, 240, 64);
			drawString(t.font, "THE TUMOR", -380, -280, 64);
			break;
		case 22:
			drawString(t.font, "THE TUMOR", -380, 240, 64);
			drawString(t.font, "THE VEINS", -380, -280, 64);
			break;
		case 23:
			drawString(t.font, "THE VEINS", -380, 240, 64);
			drawString(t.font, "EL CHAPO", -380, -280, 64);
			break;
		}
		drawString(t.font, "PLAYER X", -380, 200, 48);
		drawInt(t.font, p.lives, -120, 200, 48);
		drawString(t.font, "SCORE", -380, 160, 48);
		drawIntLG(t.font, p.score, -200, 160, 48);
		break;
	}
	glEnable(GL_DEPTH_TEST);
}

void initGL(){
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	// Calculate The Aspect Ratio Of The Window
	gluPerspective(60.0f, 800 / 600, 0.1f, 20.0f);
	//select the modelview matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS | GL_LEQUAL);
	//make sure the Z-Buffer doesnt apply to alpha pixels
	glAlphaFunc(GL_GREATER, 0.1);
	glEnable(GL_ALPHA_TEST);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_TEXTURE_2D);
}