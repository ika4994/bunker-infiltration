#include <math.h>
#include <stdio.h>
#include <Windows.h>
#include <stdlib.h>
#include <gl/glew.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include <al.h>
#include "main.h"
#include "entity.h"
#include "world.h"
#include "engine.h"
#include "audio.h"

//all functions related to the initialization and loading of resources

//load a 24-bit bitmap image as an openGL texture
GLuint loadBMP(char *filename,int w,int h){

    GLuint texture;
    int *data = malloc((w*h*3)*sizeof(int));

    FILE *file = fopen(filename,"rb");
    fseek(file,54,SEEK_SET);
    fread(data,w*h*3,1,file );
    fclose(file);

    glGenTextures(1,&texture);
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S,GL_REPEAT);
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,GL_REPEAT);
    gluBuild2DMipmaps( GL_TEXTURE_2D, 3, w, h,GL_BGR, GL_UNSIGNED_BYTE, data );
	
	free(data);

    return texture;
}


//load a 32-bit Truevision TGA image as an openGL texture
GLuint loadTGA(char *filename, int w, int h) {

	GLuint texture;
	int *data = malloc((w*h*4)*sizeof(int));

	FILE *file = fopen(filename, "rb");
	fseek(file, 18, SEEK_SET);
	fread(data, w*h*4, 1, file);
	fclose(file);

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	gluBuild2DMipmaps(GL_TEXTURE_2D, 4, w, h, GL_BGRA, GL_UNSIGNED_BYTE, data);

	free(data);

	return texture;
}

ALuint loadWAV(char *filename) {

	ALuint sound = 0;
	ALsizei size = 0;
	ALsizei freq = 0;
	ALvoid *data;

	FILE *file = fopen(filename, "rb");
	fseek(file, 24, SEEK_SET);
	fread(&freq, sizeof(long), 1, file);
	fseek(file, 40, SEEK_SET);
	fread(&size, sizeof(long), 1, file);
	data = malloc(size);
	fread(data, size, 1, file);
	fclose(file);

	alGenBuffers(1, &sound);
	alBufferData(sound, AL_FORMAT_MONO8, data, size,freq);

	free(data);

	return sound;
}

//initialize all structures, and preload all graphics that need to be accessed at any time at the start of the program
void initalizeStart(struct audioCache *a,struct textureCache *t,struct player *p,struct world *w){

	//initalize openGL
	initGL();

	(*a).music1 = loadWAV("data/sound/music1.wav");
	(*a).music2 = loadWAV("data/sound/music2.wav");
	(*a).music3 = loadWAV("data/sound/music3.wav");
	(*a).music4 = loadWAV("data/sound/music4.wav");
	(*a).music5 = loadWAV("data/sound/music5.wav");
	(*a).music6 = loadWAV("data/sound/music6.wav");
	(*a).music7 = loadWAV("data/sound/music7.wav");
	(*a).music8 = loadWAV("data/sound/music8.wav");
	(*a).music9 = loadWAV("data/sound/music9.wav");
	(*a).music10 = loadWAV("data/sound/music10.wav");

	(*a).aa1 = loadWAV("data/sound/aa1.wav");

	(*a).gun1 = loadWAV("data/sound/gun1.wav");
	(*a).gun2 = loadWAV("data/sound/gun2.wav");
	(*a).gun3 = loadWAV("data/sound/gun3.wav");
	(*a).gun4 = loadWAV("data/sound/gun4.wav");

	(*a).fired = loadWAV("data/sound/YOUREFIRED.wav");

    (*t).font = loadTGA("data/textures/font.tga",96,96);

	(*t).xhair = loadTGA("data/textures/gun/xhair.tga", 8, 8);

	(*t).rocket_1 = loadTGA("data/textures/enemy/r.tga", 32, 32);
	(*t).rocket_2 = loadTGA("data/textures/enemy/r_exp.tga", 64, 64);

	(*t).gun0_1 = loadTGA("data/textures/gun/gun0_1.tga", 64, 64);
	(*t).gun0_2 = loadTGA("data/textures/gun/gun0_2.tga", 64, 64);
	(*t).gun1_1 = loadTGA("data/textures/gun/gun1_1.tga", 64, 64);
	(*t).gun1_2 = loadTGA("data/textures/gun/gun1_2.tga", 64, 64);
	(*t).gun1_3 = loadTGA("data/textures/gun/gun1_3.tga", 64, 64);
	(*t).gun2_1 = loadTGA("data/textures/gun/gun2_1.tga", 64, 64);
	(*t).gun2_2 = loadTGA("data/textures/gun/gun2_2.tga", 64, 64);
	(*t).gun2_3 = loadTGA("data/textures/gun/gun2_3.tga", 64, 64);
	(*t).gun2_4 = loadTGA("data/textures/gun/gun2_4.tga", 64, 64);
	(*t).gun3_1 = loadTGA("data/textures/gun/gun3_1.tga", 64, 64);
	(*t).gun3_2 = loadTGA("data/textures/gun/gun3_2.tga", 64, 64);
	(*t).gun3_3 = loadTGA("data/textures/gun/gun3_3.tga", 64, 64);
	(*t).gun4_1 = loadTGA("data/textures/gun/gun4_1.tga", 64, 64);

	(*t).gun1_w = loadTGA("data/textures/items/m911.tga", 64, 32);
	(*t).gun2_w = loadTGA("data/textures/items/shotgun.tga", 64, 32);
	(*t).gun3_w = loadTGA("data/textures/items/m16.tga", 64, 32);
	(*t).gun4_w = loadTGA("data/textures/items/rpg.tga", 64, 32);

	(*t).oil = loadTGA("data/textures/items/oil.tga", 32, 64);
	(*t).health = loadTGA("data/textures/items/health.tga", 32, 64);
	(*t).armor = loadTGA("data/textures/items/armor.tga", 64, 64);

	(*t).enemy_1_1 = loadTGA("data/textures/enemy/isis_hgs.tga", 96, 192);
	(*t).enemy_1_2 = loadTGA("data/textures/enemy/isis_hgs_atk.tga", 96, 192);
	(*t).enemy_2_1 = loadTGA("data/textures/enemy/isis_sgs.tga", 96, 192);
	(*t).enemy_2_2 = loadTGA("data/textures/enemy/isis_sgs_atk.tga", 96, 192);
	(*t).enemy_3_1 = loadTGA("data/textures/enemy/isis_ars.tga", 128, 192);
	(*t).enemy_3_2 = loadTGA("data/textures/enemy/isis_ar_atk_1.tga", 96, 192);
	(*t).enemy_3_3 = loadTGA("data/textures/enemy/isis_ar_atk_2.tga", 96, 192);
	(*t).enemy_4_1 = loadTGA("data/textures/enemy/isis_rk.tga", 96, 192);
	(*t).enemy_5_1 = loadTGA("data/textures/enemy/isis_jj.tga", 128, 192);
	(*t).enemy_6_1 = loadTGA("data/textures/enemy/isis_kt.tga", 96, 192);
	(*t).enemy_6_2 = loadTGA("data/textures/enemy/isis_kt_atk.tga", 96, 192);
	(*t).enemy_7_1 = loadTGA("data/textures/enemy/ec.tga", 96, 192);
	(*t).enemy_7_2 = loadTGA("data/textures/enemy/ec_atk_1.tga", 96, 192);
	(*t).enemy_7_3 = loadTGA("data/textures/enemy/ec_atk_2.tga", 96, 192);

	(*t).brick1 = loadBMP("data/textures/world/brick1.bmp", 64, 64);
	(*t).brick2 = loadBMP("data/textures/world/brick2.bmp", 64, 64);
	(*t).brick3 = loadBMP("data/textures/world/brick3.bmp", 64, 64);
	(*t).brick4 = loadBMP("data/textures/world/brick4.bmp", 64, 64);
	(*t).brick5 = loadBMP("data/textures/world/brick5.bmp", 64, 64);
	(*t).brick6 = loadBMP("data/textures/world/brick6.bmp", 64, 64);
	(*t).brick7 = loadBMP("data/textures/world/brick7.bmp", 64, 64);
	(*t).brick8 = loadBMP("data/textures/world/brick8.bmp", 64, 64);
	(*t).brick9 = loadBMP("data/textures/world/brick9.bmp", 64, 64);
	(*t).brick10 = loadBMP("data/textures/world/brick10.bmp", 64, 64);

	(*t).metal1 = loadBMP("data/textures/world/metal1.bmp", 64, 64);
	(*t).metal2 = loadBMP("data/textures/world/metal2.bmp", 64, 64);
	(*t).metal3 = loadBMP("data/textures/world/metal3.bmp", 64, 64);
	(*t).metal4 = loadBMP("data/textures/world/metal4.bmp", 64, 64);
	(*t).metal5 = loadBMP("data/textures/world/metal5.bmp", 64, 64);
	(*t).metal6 = loadBMP("data/textures/world/metal6.bmp", 64, 64);
	(*t).metal7 = loadBMP("data/textures/world/metal7.bmp", 64, 64);
	(*t).metal8 = loadBMP("data/textures/world/metal8.bmp", 64, 64);
	(*t).metal9 = loadBMP("data/textures/world/metal9.bmp", 64, 64);

	(*t).rock1 = loadBMP("data/textures/world/rock1.bmp", 64, 64);
	(*t).rock2 = loadBMP("data/textures/world/rock2.bmp", 64, 64);

	(*t).guts1 = loadBMP("data/textures/world/guts1.bmp", 64, 64);
	(*t).guts2 = loadBMP("data/textures/world/guts2.bmp", 64, 64);
	(*t).guts3 = loadBMP("data/textures/world/guts3.bmp", 64, 64);

	(*t).post1 = loadBMP("data/textures/world/poster1.bmp", 64, 64);
	(*t).post2 = loadBMP("data/textures/world/poster2.bmp", 64, 64);
	(*t).post2 = loadBMP("data/textures/world/poster3.bmp", 64, 64);
	(*t).post3 = loadBMP("data/textures/world/poster4.bmp", 64, 64);

	(*t).trump1 = loadBMP("data/textures/world/trump1.bmp", 64, 64);
	(*t).trump2 = loadBMP("data/textures/world/trump2.bmp", 64, 64);
	(*t).trump3 = loadBMP("data/textures/world/trump3.bmp", 64, 64);
	(*t).trump4 = loadBMP("data/textures/world/trump4.bmp", 64, 64);
	(*t).trump5 = loadBMP("data/textures/world/trump5.bmp", 64, 64);
	(*t).trump6 = loadBMP("data/textures/world/trump6.bmp", 64, 64);

	(*t).title = loadBMP("data/textures/title.bmp", 800, 600);
	(*t).title2 = loadBMP("data/textures/title2.bmp", 800, 600);

	(*t).story1 = loadBMP("data/textures/story1.bmp", 800, 600);
	(*t).story2 = loadBMP("data/textures/story2.bmp", 800, 600);
	(*t).story3 = loadBMP("data/textures/story3.bmp", 800, 600);
	(*t).story4 = loadBMP("data/textures/story4.bmp", 800, 600);
	(*t).story5 = loadBMP("data/textures/story5.bmp", 800, 600);
	(*t).story6 = loadBMP("data/textures/story6.bmp", 800, 600);
	(*t).story7 = loadBMP("data/textures/story7.bmp", 800, 600);

	(*t).end1 = loadBMP("data/textures/end1.bmp", 800, 600);
	(*t).end2 = loadBMP("data/textures/end2.bmp", 800, 600);
	(*t).end3 = loadBMP("data/textures/end3.bmp", 800, 600);

	(*t).intermission = loadBMP("data/textures/intermission.bmp", 800, 600);

	(*t).sky1 = loadBMP("data/textures/sky1.bmp", 800, 600);

	(*t).death1 = loadBMP("data/textures/death1.bmp", 800, 600);
	(*t).death2 = loadBMP("data/textures/death2.bmp", 800, 600);

	(*t).time1 = loadBMP("data/textures/world/timerift.bmp", 64, 64);
	(*t).time2 = loadBMP("data/textures/world/trippy.bmp", 64, 64);

	(*p).collision = 0;
	(*p).health = 100;
	(*p).armor = 0;
	(*p).x = 1;
	(*p).z = 1;
	(*p).vel = 0.0025;
	(*p).tvel = -0.00000006;
	(*p).theta = 270;
	(*p).weapon = 1;
	(*p).msd = 0;
	(*p).fps = 0;
	(*p).guntexture = 0;
	(*p).msc = 0;
	(*p).ypos = 0;
	(*p).usrc = 0;
	(*p).lives = 3;
	(*p).exit = 0;
	(*p).level = 1;
	(*p).ammo = malloc(4 * sizeof(int));
	(*p).lscore = 0;

	(*p).ammo[0] = 25;
	(*p).ammo[1] = 0;
	(*p).ammo[2] = 0;
	(*p).ammo[3] = 0;

	(*p).score = 0;

	(*w) = readWorld("data/maps/p1a1.map");

}

void getSpawn(struct player *p, struct world w) {
	for (int a = 0; a <w.w; a++)
		for (int b = 0; b < w.h; b++)
			if (w.tile[a][b] == 91) {
				(*p).x = (a * 0.5);
				(*p).z = (b * 0.5);
			}
}

struct rocket *initalizeRocket(struct enemy **e, struct world *w) {

	(*w).rocketc = 1;

	for (int a = 0; a < (*w).enemyc; a++) 
		if((*e)[a].weapon == 4 || (*e)[a].weapon == 6)
			(*w).rocketc += 1;

	printf("%d\n", (*w).rocketc);

	struct rocket *r = malloc((*w).rocketc*sizeof(struct rocket));

	r[0].owner = -1;
	r[0].x = 0;
	r[0].z = 0;
	r[0].theta = 0;
	r[0].texture = -1;

	int rCount = 1;

	for (int a = 0; a < (*w).enemyc; a++) {
		if (((*e)[a].weapon == 6 || (*e)[a].weapon == 4) && (*e)[a].rocket == 0) {
			r[rCount].owner = a;
			r[rCount].x = 0;
			r[rCount].z = 0;
			r[rCount].theta = 0;
			r[rCount].texture = -1;
			(*e)[a].rocket = 1;
			rCount++;
			printf("rocket %d assigned to enemy %d\n", rCount, a);
		}
	}
	

	return r;

}

struct enemy *initalizeEnemy(struct world *w) {

	(*w).enemyc = 0;

	for (int a = 0; a <(*w).w; a++)
		for (int b = 0; b < (*w).h; b++)
			switch ((*w).tile[a][b]) {
			case 70:
			case 71:
			case 72:
			case 73:
			case 74:
			case 75:
			case 76:
				(*w).enemyc++;
			}
	struct enemy *e = malloc((*w).enemyc*sizeof(struct enemy));

	int c = 0;

	for (int a = 0; a <(*w).w; a++)
		for (int b = 0; b < (*w).h;b++)
			switch ((*w).tile[a][b]){
				case 70:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 40;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 1;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;
				case 71:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 70;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 2;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;
				case 72:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 100;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 3;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;
				case 73:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 130;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 4;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;
				case 74:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 250;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 5;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;
				case 75:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 400;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 6;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;	
				case 76:
					e[c].x = -(a * 0.5);
					e[c].z = -(b * 0.5);
					e[c].theta = 0;
					e[c].health = 500;
					e[c].lastx = e[c].x;
					e[c].lastz = e[c].z;
					e[c].tvel = -0.0014;
					e[c].weapon = 7;
					e[c].vel = 0.01;
					e[c].collision = 0;
					e[c].mode = 0;
					e[c].seeThing = 0;
					e[c].target = 0;
					e[c].path = 0;
					e[c].pathMEM = 0;
					e[c].pathC = 0;
					e[c].pathLEN = 0;
					e[c].texture = 0;
					e[c].msc = 0;
					e[c].attack = 0;
					e[c].sTheta = 0;
					e[c].pdist = 0;
					e[c].source = 0;
					e[c].rocket = 0;
					c++;
					break;
			}
	return e; 
}

//initalize all items in the world
struct item *initalizeItems(struct world *w, struct enemy *e) {

	//count all items and alloc them
	(*w).itemc = (*w).enemyc;

	for (int a = 0; a < (*w).w; a++)
		for (int b = 0; b < (*w).h; b++)
			switch ((*w).tile[a][b]) {
			case 60:
			case 61:
			case 62:
			case 63:
			case 64:
			case 65:
			case 66:
				(*w).itemc++;
				break;
			}

	struct item *i = malloc((*w).itemc*sizeof(struct item));

	//initalize all items that will be dropped by enemies
	for (int a = 0; a < (*w).enemyc; a++) {
		i[a].taken = 1;
		i[a].type = e[a].weapon;
		if (e[a].weapon == 4) {
			i[a].type = 6;
		}
		i[a].x = -e[a].x;
		i[a].z = -e[a].z;
		i[a].texture = 0;
	}

	//item counter
	int itemC = 0;

	//initalize all items that will be placed around the world
	for (int a = 0; a < (*w).w; a++)
		for (int b = 0; b < (*w).h; b++)
			switch ((*w).tile[a][b]) {
			case 60: //oil
				i[(*w).enemyc + itemC].type = 0;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				i[a].texture = 0;
				itemC++;
				break;
			case 61: //handgun
				i[(*w).enemyc + itemC].type = 1;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				itemC++;
				i[a].texture = 0;
				break;
			case 62: //shotgun
				i[(*w).enemyc + itemC].type = 2;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				itemC++;
				i[a].texture = 0;
				break;
			case 63: //assault rifle
				i[(*w).enemyc + itemC].type = 3;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				itemC++;
				i[a].texture = 0;
				break;
			case 64: //health kit
				i[(*w).enemyc + itemC].type = 4;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				itemC++;
				i[a].texture = 0;
				break;
			case 65: //armor vest
				i[(*w).enemyc + itemC].type = 5;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				itemC++;
				i[a].texture = 0;
				break;
			case 66:
				i[(*w).enemyc + itemC].type = 6;
				i[(*w).enemyc + itemC].taken = 0;
				i[(*w).enemyc + itemC].x = (a * 0.5);
				i[(*w).enemyc + itemC].z = (b * 0.5);
				itemC++;
				i[a].texture = 0;
			}

	return i;
}

//initalize all the nodes in the world
struct node *createGraph(struct world *w) {
	
	//getting the amount of nodes in the current level

	int nodeC = 0; //node count
	
	for (int a = 0; a < (*w).w; a++)
		for (int b = 0; b < (*w).h; b++)
			if ((*w).tile[a][b] == 90) {
				nodeC++;
			}

	struct node *n = malloc(nodeC*sizeof(struct node));
	(*w).nodec = nodeC;


	//initalizing nodes

	nodeC = 0;

	for (int a = 0; a < (*w).w; a++)
		for (int b = 0; b < (*w).h; b++)
			if ((*w).tile[a][b] == 90) {
				n[nodeC].x = (a * 0.5);
				n[nodeC].z = (b * 0.5);
				n[nodeC].player = 0;
				n[nodeC].memVN = 0;
				n[nodeC].vNode = 0;
				n[nodeC].vNodeD = 0;
				nodeC++;
			}
	
	//build the node graph
	double theta = 0;
	int temp = 0;

	for (int fNode = 0;  fNode < nodeC; fNode++) {
		
		//first pass - memory allocation
		for (int tNode = 0; tNode < nodeC; tNode++) { //loop through all the other nodes and see if any of them can be connected to
			
			if (tNode != fNode) { //make sure it doesnt connect to itself

				//test all nodes to see if any are visible
				theta = atan2(n[fNode].z - n[tNode].z, n[fNode].x - n[tNode].x);
				
				temp = vecTest(n[fNode].x, n[fNode].z, n[tNode].x, n[tNode].z, theta, w, 1, 0);

				if (temp == 1) {
					n[fNode].memVN++; //increment the amount of memory to allocate / free for the node	
				}
			}
		}
		n[fNode].vNode = malloc(n[fNode].memVN*sizeof(int));
		n[fNode].vNodeD = malloc(n[fNode].memVN*sizeof(double));

		int vNodeC = 0; //counter to make sure the valid nodes array is filled up

		//second pass - filling in the data
		for (int tNode = 0; tNode < nodeC; tNode++) { //loop through all the other nodes and see if any of them can be connected to

			if (tNode != fNode) { //make sure it doesnt connect to itself

				//test all nodes to see if any are visible
				theta = atan2(n[fNode].z - n[tNode].z, n[fNode].x - n[tNode].x);

				if (vecTest(n[fNode].x, n[fNode].z, n[tNode].x, n[tNode].z, theta, w, 1, 0) == 1) {

					//tell the focus node the ID numbers of all the nodes that it has a path to - valid target nodes
					n[fNode].vNode[vNodeC] = tNode;  

					//get the distance between focus node and target node using the distance formula
					n[fNode].vNodeD[vNodeC] = sqrt( ((n[fNode].x - n[tNode].x) * (n[fNode].x - n[tNode].x)) + ((n[fNode].z - n[tNode].z) * (n[fNode].z - n[tNode].z)) );

					vNodeC++; 
				}
			}
		}
	}
	return n;
}

//destroy the node graph
void freeGraph(struct node *n, struct world w) {

	//getting the amount of nodes in the current level

	int nodeC = 0; //node count

	for (int a = 0; a < w.w; a++)
		for (int b = 0; b < w.h; b++)
			if (w.tile[a][b] == 90)
				nodeC++;

	//free all valid nodes and distances
	for (int a = 0; a < nodeC; a++) {
		free(n[a].vNode);
		free(n[a].vNodeD);
	}

	//free the remaining variables in the struct
	free(n);
}