#include <stdio.h>
#include "world.h"

//all functions related to the game world are here

//turn an int into an ASCII char
char makeASCII(int n){
	char c = n + '0';
	return c;
}

int makeINT(int n){
	int c = n - '0';
	return c;
}

struct world readWorld(char *filename){
	
	struct world w;
	FILE *file = fopen(filename, "rt");

	w.enemyc = makeINT(getc(file)) * 10;
	w.enemyc += makeINT(getc(file));
	w.w = makeINT(getc(file))*10;
	w.h = makeINT(getc(file))*10;

	w.tile = malloc(w.w*sizeof(int*));
	for (int a = 0; a < w.w; a++)
		w.tile[a] = malloc(w.h*sizeof(int*));

	for (int a = 0; a < w.w; a++)
		for (int b = 0; b < w.h; b++)
			w.tile[a][b] = makeINT(getc(file));

	fclose(file);

	w.nodec = 0;
	w.itemc = 0;
	
	return w;
}

void freeWorld(struct world w){
	for (int a = 0; a < w.w; a++)
		free(w.tile[a]);
	free(w.tile);
}