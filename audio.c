#include <stdio.h>
#include <al.h>
#include <alc.h>
#include "main.h"
#include "audio.h"
#include "entity.h"


void playSound(ALuint source, ALuint sound,double x,double z) {

	alSourcei(source, AL_BUFFER, sound);

	alSourcef(source, AL_PITCH, 1);
	alSourcef(source, AL_GAIN, 1);
	alSource3f(source, AL_POSITION, x, 0, z);
	alSource3f(source, AL_VELOCITY, 0, 0, 0);
	alSourcei(source, AL_LOOPING, AL_FALSE);

	alSourcePlay(source);
}