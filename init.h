#include <gl/gl.h>
#include "main.h"
#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED

void initialize();
void initalizeStart(struct audioCache *a,struct textureCache *t, struct player *p, struct world *w);
GLuint loadBMP(char *filename,int w,int h);
struct enemy *initalizeEnemy(struct world *w);
struct node *createGraph(struct world *w);
struct item *initalizeItems(struct world *w, struct enemy *e);
struct rocket *initalizeRocket(struct enemy **e, struct world *w);
void freeGraph(struct node *n, struct world w);

#endif // INIT_H_INCLUDED
